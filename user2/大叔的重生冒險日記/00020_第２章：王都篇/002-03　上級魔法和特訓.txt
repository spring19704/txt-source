到了在異世界的第12天。發生了那樣的事的第二天，我以乾脆的表情去了公會。為了學習下一個魔法。到了這個年齡，臉皮也變得厚了。

在接待處詢問了前幾天的接待小姐，是否有回復和攻擊，以及防禦系的上級魔法嗎？

「有喔。」
雖然這麼回答很好，但還是得面對吉爾馬斯的速攻。

現在，我昂首挺胸地坐在公會長辦公室的沙發上，和他見面。

「算了，就這些了。別那麼不高興。那個藥水也是軍事物資。軍方也會把他們召集到沙加利基。而且那個軍閥也兼任貴族的派系。說如果我家也進貨了就算了。雖然也可以置之不理，但是我們也有不得不給政府機關相關文件的情況，也和那方面的情況有關聯。政府機關裡貴族的文官也占了很大比重。

不只是那個。現在是受到國家保護的有名的藥師製作的，但是數量不夠。難怪貴族們對國家沒有幫助，但除此之外的管道則另當別論。壓制生產商就會產生巨大的利益。抱持有奇怪想法的貴族，也應該在裡面。你沒有在其他地方賣吧？被盯上的話，就白饒了你？」

啊，糟糕。

「算了，我們不會再要求批發了，絕對不要散布出去。如果你珍惜生命的話。」
「啦呀……。無論如何都想要的時候告訴我。部分給你吧。但出處要保持沉默。」

吉爾馬斯微微一笑，就用黑色的笑容說了出來。

「這樣啊。我想要100支左右。」
我乾脆地拿出100支。吉爾馬斯偷偷地拿來了50枚大金幣。

「那麼，是魔法吧。我特別教你吧。跟我來。」
就那樣被帶去練習場，發生了這樣的事。

「記起來挺好的。把剛才的大金幣都拿出來。」
這傢伙，真是個鬼啊……我以死心的臉拿出來還給你。

「不要擺出那種表情。但是從現在開始只要是喜歡的，都可以學。想長壽的話，就拚命學吧？特別是你的情況。」

嗚，完全被當成了問題兒童了。沒有辦法否定。

吉爾馬斯毫無顧忌地放出了上級魔法。


【閃光】（爆發系大型火魔法）
【重力】（拘束用的重力魔法）
【暴風雨】（超強力人工風暴）
【雷雨】（超雷擊之風暴）
【衝擊波】（超高熱的熱線魔法）
【米吉多】（消滅之光）
【流星雨】（超隕雨）， 
【龍捲風】（Ｆ６級大龍捲風）
【水波】（大海嘯）
【大地拘束】（巨大拘束土枷）


不管怎麼說，魔法排成一排。這已經夠本了。價值相當於1000枚金幣。

真靈巧啊。這樣的高級大魔法在這麼狹小的地方能靈活運用。一般是不可能的吧。美泰奧林頓什麼的很藝術。從天花板上降下最小威力的東西，使之咕嘟咕嘟地吞沒著地板。

話雖如此，自己在那一帶也做得很好。因為原來的魔法很優秀啊。做錯事本來就很拿手。

吉爾馬斯露出有點佩服的表情。因為在這樣的房間裡，用自己的魔力量不加控制的話，是自殺行為。

吉爾馬斯也是等級高的冒險者。怎麼說呢，所以才是吉爾馬斯吧。


【保護】、【發現攻擊】、【發現保護】、【發現溫度】、【發現衝擊】
【減輕】、【加重】、【解咒】、【反解咒】、【懸浮】
【飛行】、【風盾】、【命中補正】


吉爾瑪斯啊，一開始就打算用上級魔法攻擊我嗎？


【盾】、【魔法盾】、【屏障】、【風牆】
【火牆】、【冰壁】、【高級盾】、【高級魔法盾】
【高級屏障】、【土牆】
【魔法劍】、【炎】、【冰】、【風】、【土】、【雷】

【陷阱察覺】、【陷阱解除】、【探索】、【索敵】、【索敵探知】、【沉默】、【聲納】、【隠密】、【気配遮斷】、【気配察知】、【隱形】


多才多藝的男人！這傢伙，難道是稀人？稀人未必都是日本人。過幾天，試試看能不能問出來。也有可能是轉生者。

「怎麼樣，很划算吧？但是，一次就學會了？」
「算了。太感謝了。」

「還有，劍什麼的好可愛啊？訓練的形式啦告訴自己要做。我不想死。靠近而疲憊不堪的魔法師真是不勝枚舉。」

從那裡開始不知為何變成了好好地排練。不過，想辦法處理了。因為發現是能看見魔法發動的系統，對這樣的東西不有用。

還有一點接受委託，被要求提高等級。一旦有E等級被舔了就變得不利了。

已經到了傍晚，筋疲力盡地回去了。身體強化上升到Lv9，成為了100萬HP。就那樣趴在床上了。

為什麼只有MP需要那麼多的補正呢，想著無關緊要的事情就困了，就這樣睡著了。

在異世界的第13天。不管怎麼說，公會長很會照顧人，他應該會很忙吧。

對了，昨天沒有吃飯！早上吃了咖哩。雖然是袋裝食物。幹勁十足地吃了兩杯。如果是年輕人的話，能吃5杯左右吧。

但是學會了飛行，真是太好了。萬一發生事故時，出現逃亡手段，令人高興。那麼，有錢。得到了保護身體的魔法。武器也得到了最強等級。也有逃亡的手段。

之後要做的事，是提高魔法LV，和劍術的訓練嗎？還有就是為了盡情享受王都而來的。

啊，找回原來世界的手段。忘記它了。已經完全染上了這個世界。我本來就是家裏蹲，只在必要範圍外出。

有需要的東西隨身攜帶，也能品嘗到日本的食物。身處在這個世界就是逃避現實了。

與魔物戰鬥，克服盜賊的襲擊，才變得如此自大。

好吧？在這個國家，有稀人傳播的沐浴文化，食物也很合口味。即使回到日本，也沒有想見的人。

因為網路相通，更加如此。如此幸運的異世界生活也並非如此。年邁的大叔，加速了這一切。

無論做什麼都只能打發時間。總之，在挑戰晉級之前不鍛鍊的話就會死掉的。對了對了，還不能和魔物正面接觸。

然後來到了公會。今天窗口也空著。

我問一下昨天接待處的人。
「吉爾馬斯在嗎？」

「嗯，在啊。」
那真是太幸運了。

「能見面嗎？」
「我帶您去吧。有什麼事嗎？
「我有想問的事情。」

我馬上帶我去了辦公室。
「吉爾馬斯，阿爾馮斯先生來了。可以嗎？」
「啊，讓我過去。」

「你好！」
「怎麼了？」

「我有想問的事情。那個，魔物很強嗎？我只見過一次E級的傢伙。」

「對了，他說幾乎沒有見過。難道是本能性地感受到你的魔力量，而避開你了嗎？」

「什麼？作為冒險者不是致命的嗎？那個。」
那個格里昂什麼的，只是不要命嗎？是嗎，或許它餓了。

「適合做護衛任務。收納拿出來。在商會工作也是其中之一，你原本不是商人嗎？」
「啊，原來是這樣的設定啊。」

「什麼？」
吉爾馬斯略微吃驚地反駁道。

「嘛，去迷宮的話，你毫無疑問的會被攻擊。」
「有迷宮嗎？」

「連這都不知道的傢伙竟然在我們公會裡。作為吉爾馬斯真悲傷。」
「算了。那麼，在哪裡？」

「在王都近郊20公里的地方。詳細資料去問職員。再稍微鍛鍊一下劍和魔法的話，就不用慌張了。去之前來讓我看看。」
「raja。打擾了。」