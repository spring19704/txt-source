   溫德林，當爸爸了。

【嗯.....還沒有出生啊】

在阿格尼絲他們畢業之後，終於馬上要到埃莉斯她們的產期了。

作為父親的我非常想在她們的身邊陪著她們，但是這個世界裡妻子生產的房間男人是不能進去的。

霍恩海姆紅衣主教派來的助產婦和神官們，全員也都是女性。

和接生相關的男性也只有醫生，而且他們除了做事先的檢查以外什麼都不能幹。

另外，在孩子出生的時候不能進房間裡面這件事讓我很吃驚。

【還沒生嗎，還沒生啊......】

在分娩的房間外面，我也只能一味的等待了。

實在是太著急，太無所事事了，於是我把附近的窗戶都打開來透透氣。

於是乎，一顆巨大的樹進入到我的視野裡。

風吹過，數枚樹葉落了下來，我在它們落地之前反覆的用極小的Fireball烤著。

為什麼做這些事情，總之心裡十分焦急、什麼都做不了、擔心著停不下來。

做了魔法特訓的同時也讓焦躁變得平靜，一石二鳥的事情。

沒有其他的意義。

【取名叫Firegun ，葉子已經掉不下來了嗎......】

很快落葉就用完了，我又回到椅子上坐著抖腿了。

活到今天為止就習慣那種事情是不可能的，這樣一來反而有種安心了的感覺。

【唔......】

但是現在也不能馬上讓心情平靜下來，這次從魔法袋裏面拿出師父留下來的書讀了起來。

雖然說師父留下來的書大概都看過了一遍，但是我想自己忽略了一部分關於分娩的知識和魔法也說不定。

【沒有...和生產相關的魔法...啊，師父是男人...單身...】

但是，有了新的發現。

已經是十多年前獲得的書了，一字一句讀的都很認真賣力。

【接下來...】

又煩躁起來了，這次取出來空的魔晶石試著向它補充魔力。

【魔晶石在閃閃發光。真漂亮啊...不！這不是當然的嗎！！！】

補充魔力的話魔晶石就會閃閃發光，這是常識。

【溫爾，趕緊靜下來】

【不，但是啊...】

【就算你再怎麼著急，情況也不會有什麼變化的哦】

和我一起等著埃莉斯生產的埃爾平靜的提醒著我。

【是不是有點慢啊】

【產房裡才不到兩個小時哦。埃莉斯是第一胎吧，時間也會稍微久一點，不是嗎？】

埃爾有生育的相關知識。

我，到現在為止的生活一直和生育什麼的無緣啊.....

在前世有一個小一歲的弟弟，但是他出生時候的事情完全不記得。

在這個世界裡我是最小的弟弟，艾梅莉大嫂分娩的時候，因為我是鄉下領主的妨礙被趕走了啊。

老實說，並不明白。

【埃爾，說詳細點。】

【老家的領地裡定時就會有孕婦分娩的，幫忙什麼的說不上，但是產婆需要半天甚至一天才能趕到的情況非常多。很快的人也有的啊

原來如此，在老家看過領地居民的生產啊。

我忙著做魔法的練習，並沒有關心這類的事情啊。

【溫爾，喝了這個冷靜一下】

埃爾說話的同時，穿著女僕服的艾梅莉大嫂端給了我盛著馬黛茶的杯子。

【在這個時候，不拿出來做父親的架勢是不行的哦！】

【這樣嗎？】

【是啊，只要靜靜的坐在那裡就好了。】

【我知道了】

多虧了艾梅莉大嫂的忠告，我總算是平靜下來了。

【哎呀哎呀，艾梅莉桑在真是太好了。】

時間還在前進，已經過去幾小時了呢？

我和埃爾，只能一味的等著。

這個時候，吸煙的話或許能緩解一下吧。

在前世還是大學生的時候試著吸了一下，不過非常嗆人非常不適合我。

這個世界裡並沒有香煙，自己不做的話就沒有。

不，找一找或許會有的吧，但是我並不知道煙草葉。

不是食物的話並沒有興趣去找，在○Ｔ就職的話或許會知道的更詳細吧。（這裡應該是指JT，Japan Tobacco  日本煙草公司，世界上第三大跨國煙草公司）

嗯，那麼超一流的企業，和我沒什麼緣分啊。

哎呀，話題扯遠了。

【布蘭塔庫先生也不在，先喝酒也不行啊......】

【就算是布蘭塔庫先生，這個情況下也喝不進去酒的吧......】

【不知道哦】

不管怎麼說，那是所有東西里最愛酒的布蘭塔庫先生啊。

在這個地方開一場酒宴，我也並不覺得不可思議。

【等等，用酒來排解焦慮這種手法......】

【這太不嚴肅了所以趕緊停下來！】

在埃爾的提醒下注意到在自己孩子出生的時候擺酒宴的傢伙並不存在。馬上在埃莉斯的產房裡，大大的嬰兒的哭聲傳了出來。

終於，我的孩子出生了啊。

【哇.......！哇.......！】

【溫爾！】

【埃爾！】

【出生了啊！】

【啊啊！】

我很慌張的想要打開房間的門，發現門還被牢牢的鎖著。

【啊.....，已經可以了吧？】

【啊，是，馬上給您打開。】

我發出了打開門的命令，在產房裡聽到了助產婦的聲音。

因為接生慌張的助產婦門正不安著吧，這樣的態度也正常呢。

她發出緩緩的的聲音把門打開了。

馬上門就打開了，我一點點拉開門縫的瞬間，房間裡發出了奪目的光芒。

【魔法的光？】

【什麼？這麼耀眼？】

慌忙的和埃爾一起進到房間裡，在我們的視線裡散發光輝的嬰兒出現了。

果然剛才炫目的魔法之光是由埃莉斯剛出生的小孩子發出來的啊。

【不愧是我的孩子，已經能發出這等光芒了啊。不，散發出和我不同的超凡領袖魅力。】

【已經完全是笨蛋爸爸了，話說回來，普通的嬰兒會這樣發光嗎？絶對是什麼魔法的效果在哦。】

嬰兒的光輝大概一分鐘就消失了，此時馬上做魔力的檢測，結果我剛出生的孩子就已經差不多有初級魔法使能使用的魔力量了。

【沒想到，歐內斯特指出來我的孩子有很高的概率是魔法使的事情瞬間就變成現實了啊。】

【埃莉斯，是閃閃的有精神的男孩子哦。做的真棒。】

孩子現在很精神，首先先給初次分娩而疲勞的埃莉斯施展治癒魔法並和她說說話。

其實是能使用的，但是比埃莉斯的手段低所以平時也沒什麼機會使用的治癒魔法久違的用出來了。

【是男孩子就安心了】

不會有錯的，埃莉斯的老家也在期待著男孩子吧。

這等大事也有了個結果，這樣她的臉上浮現出安心了的表情。

我呢，不論是男孩女孩只要順利的出生就好了。

【鮑邁斯特大人，埃莉斯大人初次生產是安產可以放心了。分娩時為了緩和疼痛的治癒魔法也已經使用完畢了。】

霍恩海姆紅衣主教派來的超老手的助產婦，也比預定的要早的告訴了我。

為了慎重起見在場有複數的能使用治癒魔法的神官在待命，但是貌似沒什麼機會出場。

難產的話神官就會反覆的使用治癒魔法，在這個世界裡這是接生時的常識。

只不過，需要能傳喚能使用治癒魔法使的人這樣的條件。

普通人的話就只能和以前的日本一樣背負著風險去生孩子。

【是嘛，這真是太好了。】

完美超人的埃莉斯，分娩的時候表現的也那麼完美。

【真可愛啊，頭髮和我是一樣的顏色啊...】

現在還有點像猴子似得，但是寶寶身上繼承了鮑邁斯特家族特徵一樣的濃茶色的頭髮。

這孩子過幾年之後一定會是個帥哥。

男孩子的話會和母親比較像，不會變成我這樣的小鬼吧。

【親愛的，給孩子取名就拜託你了。】

【是啊...候選稍微有點多，不過會好好決定的。】

在很久之前就已經準備取名字了，不過現在還在猶豫著選哪個名字。

打算用哪個，哪個更好，用這樣的心意在思考著。

這一週左右，我一直是這個樣子。

【拜託你了】

【霍恩海姆紅衣主教，關於孩子起名字說什麼了嗎？】

可愛的孫女要生產，於是他把王都內數一數二優秀的接生婆和治癒魔法使的神官派過來了。

她們是王族生產都會被點名的擁有很高的手腕的人。

做到這種程度了的話，（紅衣主教）想自己命名也說不定。

【爺爺說，這個孩子的名字一定要你來取，因為這孩子將會是鮑邁斯特伯爵的繼承人啊。】

就算是自己曾外孫，這個孩子也是其他伯爵家的下任當家。

霍恩海姆紅衣主教來取名字，確實道理上說不過去啊。

【溫爾，還沒有決定嗎？】

【很難確定下來一個啊。】

【名字數日後再起也不遲，但是發出了很厲害的光吧？這孩子】

出生的孩子做下任鮑邁斯特伯爵這件事已經是既定事項了。

比起這個，魔法師的特性本來不應該遺傳的，但是這孩子繼承了這一點。

出生時光芒四射，迷一樣的發光現象。

溫德林啊，你出生的時候有發光嗎？

父親聽到的話，自尋煩惱一樣的可怕啊。

【埃莉斯啊，這樣的事情在哪裡聽說過嗎？】

【不，第一次聽說。】

埃莉斯立刻開始給剛出生的孩子喂奶了。

看起來喝的相當賣力。

埃爾在這之前從房間裡走出去了。

不能看主君妻子的胸，作為埃莉斯的朋友也應該留心的吧。

【溫爾，可也嗎？】

【啊 】

在埃莉斯初次哺乳之後，埃爾帶人回來了。

這是我家裡危險的客人，魔族的考古學家歐內斯特。

他寫了有關一個月前重新探索地下遺蹟的論文，這些天一直在吃罐頭。

【鮑邁斯特伯爵，夫人殿下，生產順利可喜可賀】

感覺說不定意外失禮，歐內斯特也有恭喜我們的常識啊。

在此之前，忽然指出我的特殊性並弄出各種各樣的混亂，讓我知道了過去的存在。

即使歐內斯特的推論錯的話，偶爾有學者闡述新的トンデ學說提到，實際上是有存在魔力的嬰兒存在的。

作為下一代鮑邁斯特伯爵，這孩子會很辛苦吧，我也有了會有各種各樣辛苦的的預感。

【正如歐內斯特所說，學者特有的トンデ新理論馬上就要完成了】

【鮑邁斯特伯爵，就算是天才的吾輩也會有發佈錯誤的論文的可能性的。但是，人工魔法使的理論是從遠古時代的專業學者就研究著的，結論也是確定了的。是不會有錯誤的，所以放棄吧。】

【是啊...】

【古代魔法文明時代的人們，用和我們魔族一樣的魔法才能讓魔法才能人工遺傳。然而魔族和人類的身體相差太多，持久性的遺傳要素研究失敗了。因為作為人類和魔法相關的生物藍圖的繼承能力很弱。研究得出的是這樣的結果。】

但是生物藍圖...遺傳因子（基因）已經被認知了。古代魔法文明時代的科學力量果然不可小覷。

【那麼，混血是什麼情況呢？】

【以前，留下了一些記錄，】

人類和魔族的混血的孩子

兩者都看起來像魔族一樣微長的耳朵，其他方面並沒有什麼大差別。

但是同是混血的兩個人不能生孩子。

與魔法有關的遺傳基因固定化的話，同是混血的兩個人最有效率。

然而，混血之間並不能有孩子。

【混血和人類生的孩子是人類，混血和魔族生的孩子則變成魔族了。】

所以說，這個大陸上並沒有混血是吧。

魔族住在遙遠的西方，一萬年以上的時間沒有交流了。

【對於古代文明實在人們是使用何種方法讓大部分的子孫成為魔法使這件事情，詳細的方法並沒有流傳下來。】

人工魔法使在古代魔法文明毀滅的時候就一同消失了啊，只是非常偶然的在我身上發生返祖現象而復活了啊。

【不可思議的事情也有啊，但是為什麼就這樣斷言我就是呢？如果真是這樣的話，出生之後家人難道注意不到麼？】

剛剛出生的小寶寶，光芒那樣的炫目。

雙親不可能注意不到。

不過，剛出生的溫德林持有的魔力不明。

【生物藍圖在產後是不可能發生變化的，鮑邁斯特伯爵魔法使的才能覺醒的時候是在五歲到六歲的時候。在那個時候發生了什麼呢？】

【不，特別的什麼並沒有】

【嗯.......很神秘啊。】

五到六歲，是我附身到溫德林身上的時候。

在靈魂替換的時候，溫德林的遺傳因子發生了什麼變化嗎？

這樣想起來，總算是接受一部分了。

不論哪邊都是推論而已，真相或許兩者都不是。

在此之前，我還有必須要做完的事情。

【名字啊......候補太多了】

【你那個才是大煩惱啊。】

【名字是很重要的哦，要跟著那個孩子一生的。】

因此，是不能採用那種閃閃發亮的那種名字。

我從魔法袋裏面取出赫爾穆特王國人名事典的homepage開始讀。

總而言之，不給這個剛出生的孩子好好起名是不行的。

【在心裡有幾個候補選項了嗎？】

【現在好名字暫時還沒有想出來......】

我現在還在為孩子的名字深深的煩惱著。

【埃莉斯，生了呢。】

【男孩子啊，真是太好了。】

同樣待產的伊娜和露易斯頭一次進這個房間來，看到埃莉斯生的是男孩子就安心了。

如果這個是女孩子，然後伊娜和露易斯的孩子是男孩子的話，那麼繼承權的糾紛就會增加了。

【家世上有困難的，但是有人想通過騷亂突破這個困難，】

【布萊希萊德伯爵的一部分家臣之類的】

和埃莉斯的孩子相比，伊娜和露易斯的孩子在思想和動作上更容易控制。

話不能全這麼說，就算是布萊希萊德伯邊境爵家也不會全是正直的家臣，完美的統治也是不可能的。

一定會有暴走的人出現。

所以，有埃莉斯生的嫡子就可以安心了。

【貴族啊，真是麻煩的要死啊。】

【比起這個，現在還在為起名字煩惱著嗎？】

【埃爾怎麼認為？】

遙也快到生產期了，所以埃爾也必須要考慮孩子的名字了。

【男孩子的話就叫里昂（レオン），女孩子的話就叫艾瑪（エマ）】

哎呀，埃爾已經決定下來孩子的名字了。

這麼早就決定下來，說實話很羡慕。

【用什麼樣的標準呢？】

【呀，韋爾拿著的書隨便的卷著的呢。】

眼前停留的名字應該是不會迷惑的選擇了。

相當的合適，感覺很不錯的名字。

【這種事情煩惱起來就開始沒完沒了了。鮑邁斯特家傳統的名字是什麼來？】

【沒有......】

埃爾，在鮑邁斯特家期待著什麼的吧？

傳統的名字什麼的，雙親和哥哥們也都沒聽說過。

【我們的老家雖然有的，嫡子幾代前的當家的名字。】

這樣的不只是埃爾的老家，相當多的貴族家裡也這樣實踐著。

只有家主能傳下去的名字，繼承它的孩子就可以向周圍宣稱繼承家主。

【但是如果嫡子有什麼事情而次男出場的回合呢？】

【次男也用數代前當家的名字，所以，有備用使用的。】

【使用當家的名字，住在預備的房間嗎......】

這個世界裡次男的待遇這麼低，我不禁同情了起來。

受到優待的只有名字啊......

【但是因為我是八男所以可能不知道，實際上鮑邁斯特家可能有秘密的命名標準也說不定。】

這樣想的我為了和父親聯繫用魔導手機聯絡了保羅兄長，報告說埃莉斯生了一個男孩子。

【生下來的是男孩子嘛，真是太好了】

【是的，安心下來了。】

【第一個孩子，父母總會有各種各樣的擔心的。】

父親為我的兒子出生這件事而感到高興。

馬上就變成母親，她也在為生了男孩子而感到高興。

【女孩子不行這樣的事也不存在，貴族是拘泥於嗣子的傢伙啊。總是因為沒有孩子而煩惱的貴族也很多。和家族的延續息息相關啊。】

招女婿，從親族中過繼樣子從而保持家族，不過果然從血統上父母的心情還是希望兒子來繼承家業啊。

【我們鮑邁斯特家，好像一次也沒有因為生不出小孩子而為難。】

規模和經濟情況十分微妙，沒有誕生繼承人是很辛苦的事情。

說起來，哥哥們也都已經結婚，孩子也馬上就要出生了。

父親也有十個孩子，不管怎麼說剩下的孩子的出路讓家裡很煩惱吧。

【話說回來，父親，鮑邁斯特家族有決定下來的命名基準什麼的麼？】

【倒不是沒有。數代前的當家的名字。也不需要嚴格遵守什麼的......】

很意外的，鮑邁斯特家居然也有這樣的傳統。

庫爾特（クルト）和赫爾曼（ヘルマン）嗎，以前當家的名字嗎。

啊...但是庫爾特有可能在今後用來命名吧。

【溫德林。這個命名標準只用在了赫爾曼那裡而已哦，保羅是第一個，所以由自己來決定，至於赫爾穆特和埃裡希，他們是入贅的，決定權就尊重他們吧。】

這樣啊，我是初代當家，所以不決定自己孩子的名字是不行的啊。

【保羅也在苦惱著，在這個時候王國人名辭典就非常管用了。保羅也用了。作為最終手段，這樣啪啦啪啦的翻著。】

【呼......】

為孩子的名字煩惱著，依賴著王國人名辭典。

這不只是我，這對於住在赫爾穆特王國的大部分的父親而言是常規商品。

看來，我的溫德林這個名字是父親適當的翻著辭典然後在他眼前停下的名字啊。

十個人，父親也偷工減料了的樣子。

【再稍微長大一點了，我們過去讓你看孫子的臉啊。】

【好，等著你們哦。】

和父親說完話，我把魔導手機通訊切斷了。

【沒考慮好名字啊......】

既然是孩子的名字，那向裡面傾注雙親各種各樣的願望也是應該的吧。

啊嘞？

傾注父母願望的名字？

我開始感到巨大的違和感。

在內在是日本人的我看來，名字裡不使用漢字的話是很難寄託願望的。

這個世界裡人們說日語，為什麼德國風的名字比較多呢？

【（德國風的名字的意義不太明白啊......）】

德語在大學裡作為第二語言選學過，不過現在已經基本不記得了。

『グーテンモルゲン』『バウムクーヘン』......不能用作孩子的名字吧。在此之前，我的溫德林這個名字是意思本身就不太明白。也就是說這裡不轉換方針就不行啊。

【王國人名辭典要起作用了哦】

【所有的候選項都在這裡記載著嗎，看看吧。】

我拿起赫爾穆特王國人名詞典開始隨意翻開來。

在翻開的頁麵裡，一個名字在我眼前停了下來。

【就叫弗裡德里希（フリードリヒ）吧。】

【好快！】

【這個，難道不是好名字嗎？】

【確實是個好名字。】

在前世世界史的授課中，有聽過用這個名字的大王或者皇帝。

（注 弗裡德里希，又被翻譯成腓特烈。這裡指腓特烈大帝。）

而且，我也認為這是非常棒的名字。

雖然難以形容，不過我認為這個名字相當棒。

【決定了，這孩子的名字就是弗裡德里希了！】

【弗裡德里希.馮.諾.鮑邁斯特嗎。是個好名字啊。】

埃莉斯也同意了，所以就決定小寶寶的名字是弗裡德里希了。

這孩子，將作為鮑邁斯特的下一任領主來進一步發展這個領地吧。

【弗裡德里希，快點長大讓我引退吧。】

這個孩子能認真的去做個領主的話，我也就可以安心的過著自由的冒險者的隱居生活了吧。

【後代出生的感動什麼的各種各樣的被糟蹋了啊！】（「色々と台無しだよ！　跡取りが産まれた感動とかすべて！」）

【溫爾還不是才十多歲。現在不要說這種像老人一樣的話。】

【從這裡發生的各種各樣的事情上來說，我認為是不行的。】

不知為什麼被埃爾、伊娜、露易斯連續的批評了。總之平安出生真是太好了。

首先有了鬆一口氣的時間了。接下來是半個月之後，這次是伊娜她們陸續要生產了。

比起這個，鮑邁斯特伯爵家的生產高潮仍在繼續。

【這個啊，比想像中痛多了啊...】

【伊娜桑，用治癒魔法吧。】

【謝謝，埃莉斯...痛痛痛痛！】

【再稍微強烈一點】

【謝謝，輕鬆多了。】

這個世界裡分娩有些部分比日本還要舒服。

當然，有自己是治癒魔法使這樣的確保的條件。

伊娜，托埃莉斯的治癒魔法的福平安無事的生了一個女孩。

我和生弗裡德里希那時候一樣，只能等著......除此之外啥都幹不了。

【館主大人，手上有解悶的辦法哦。】

【有這麼厲害的事情呢。所以說，是什麼？】

【館主大人，是工作。】

【這邊還在生小孩子？】

【館主大人，您的孩子出生了的話我們會用魔導手機通知您，那時請用瞬間移動馬上過來。】

【羅德里希，你是魔鬼啊！】

【這也是為了即將出生的孩子們。】

只是我被在伊娜之後的生產帶來的不安頂著，而羅德里希則為我編製出了孩子出生之後的工作日程表。

全部小孩生完再回去就好了，果然羅德里希是魔鬼啊。

【為了剛出生的孩子們，館主您不努力不行啊。領地的擴展和孩子們的生活緊密相連著，孩子們都是看著父親的背影長大的啊。】

【剛出生的小孩子，看不見我的背影的。】

【......有您的工作】

【羅德里希，敷衍一下好嗎？】

羅德里希說的話是正確的，把孩子出生之前的工作極力退給主君的家臣很厲害。

我一邊進行著新城鎮所在地的徒弟平整工作，一邊等待著孩子是不是會早點出生。

然後幾小時後，終於接到了娜也順利的生出小孩子了的通訊。

【髮色遺傳自伊娜嗎。女孩子也蠻可愛的。】

其實，不論哪個都和剛剛出生的猴子一樣。

但是，馬上就變得好可愛。

【和埃莉斯那時候一樣，這孩子也很耀眼。】

【雖然有預料到，這孩子也是嗎......】

和歐內斯特的說明一樣，伊娜生出的我的女兒也發出了炫目的光。

接下來...

【我也想要女孩子啊。而且也要閃閃發亮的。】

鮑邁斯特伯爵家的出生高潮還在繼續。

在露易斯之後數月之間，卡特莉娜的是男孩子、泰蕾莎的是女孩子、維爾瑪的是女孩子、麥琪亞的是女孩子、莉薩的是女孩子。（坑爹的事情來了，因為我之前一直沒注意人名對應的日文，所以...........人名翻譯可能有錯誤  原文如下：ルイーゼの後も、數か月の間にカタリーナが男の子を、テレーゼが女の子を、ヴィルマが女の子、カチヤも女の子、リサも女の子と。）

短短數月，在我十八歲的時候就變成了兩個男孩子，六個女孩子的父親了。

但是，八個人嗎......

馬上就要超過父親了嗎.......

【女孩子稍微有點多啊、不過，弗裡德里希、凱恩（カイエン），作為少數的男孩子要加油哦。】

【溫德林桑，小寶寶為什麼要努力啊？】

【所謂男人啊需要和男人們打交道，身邊被太多女性包圍的話會很累的。】

【是這樣子的嗎？】

卡特莉娜一副搞不明白的表情，但是這是男人切實的問題啊。

孩子們中女孩子那邊會成長的比較快，也口齒伶俐。

弗裡德里希和凱恩被姐姐啊妹妹啊包圍著，很麻煩也說不定。

在日本這樣的地方就叫做【女人們中的一個男人】的狀態。

因為有兩個人，所以不會被孤立嗎。

【埃爾文桑那裡，里昂也在嗎？】

分娩結束後，剛出生的小寶寶被轉移到孩子們專用的房間裡了。

我們準備了能收容很多人的大房間，首先把相應人數的嬰兒床放在那裡，這就是他們生活的場所了。

貴族家孩子的出生和照看需要拜託的事情有很多，房間裡遙和其他家臣的夫人們都聚在一起。

只是，母乳不夠的話，基本上還是母親本人給孩子喂奶。

【埃莉斯，這樣就行了嗎？】

【貴族家需要對應亂七八糟的東西，所以我想自己給孩子喂奶。】

完全委託給奶媽的家，彌補不足份量的家，母親儘量解決的家。

選擇哪種方式一般是家族的傳統，偶爾會有貫徹自己意見的家主和妻子。

也就是說選擇適當的方式就好。

只不過目前的情況，為了代替母親們照看孩子，需要僱傭奶媽啊。】

即使是僱傭也不會用外部的人員，一大半的情況是僱傭生下孩子的時間接近的家臣妻子，我家也是把埃爾的夫人遙顧過來當奶媽的。

遙的話，也可以期待她做寶寶們的護衛。

現在瞄準我的孩子們的傢伙我想是沒有的吧，但是也是為了保險起見。

當然，遙生的里昂也一起在嬰兒床上睡覺。他已經變成弗裡德里希他們的一奶同胞了。

【乳母的孩子也一起照顧，這樣照顧孩子的效率就提升了。】

【效率嗎......】

【大家，都很忙啊。】

家臣的妻子們自己也並沒有空閒。

正如卡特莉娜所說，在一起培育的方式效率是坐高的。

之後也有讓家臣和家主的孩子們一起成長，順便培養感情，培養出忠心耿耿的家臣的目的。

正因為我們的家族是新興貴族，家主和家臣之間的聯繫不加強不行。

不只是里昂，也有把近年家臣的孩子也聚集過來的預定。

【這是個好風俗啊，不過老家沒有這樣的事啊......】

在溫德林的記憶裡，乳母啊乳兄弟啊這樣的存在完全沒有印象。

我不論到哪裡都是自己一個人。

我啊，一邊哭著一邊成長著。

【地方領主，到此為止也有顧不過來的人......】

儘管如此，家臣妻子的奶水是分開的，用同一時期孩子們之間青梅竹馬的關係而帶來忠誠心，這樣的事情正在進行著。

【但是，繼承人只有長子啊。】

和我差不多，作為五男而被放置不管的埃爾也明確表示自己沒有乳兄弟之類的。

【哦，夥伴呦！】

【很討厭的夥伴吶，遙桑，平安的生下了作為繼承人的里昂才放心了呦。】

這樣埃爾文家業安心了。了。

在我和埃爾看到的旁邊，遙正在給剛出生的小寶寶們輪番換尿布。

【遙，這不是很熟練嗎？】

這命名應該是第一次生育，遙卻已經相當習慣照顧嬰兒了。

聽理由，是因為孩子的時候就去照顧親戚家的寶寶了。

【下級的武士都是窮人吶】

因為男女不得不一起去工作，所以有把親屬之間的孩子聚集起來由年長的孩子來照看的習俗。

遙談了一邊照顧著年幼的孩子們，有時間的時候進行刀的練習，之後被拔刀隊選拔上了這樣的話題。

【不愧是刀術的天才啊。】

如果我在這樣的環境裡的話，那我肯定只會照顧嬰兒了吧。

【但是有有經驗的人在下真是感激不盡。】

其他的保姆們也是有照顧嬰兒經驗的經驗者，自己的孩子和弗裡德里希椅子好好的照顧著。

由那次相親大會結婚的家臣們的孩子也一個接著一個出生了，鮑邁斯特伯爵領裡迎來了空前的嬰兒的高峰期。

【比起這個，大問題是...埃莉斯桑的弗裡德里希也好，我的兒子凱恩也好，其他所有的孩子都......】

【繼承了母親的才能是好事嗎？】

【感覺到了在這之上的危險了啊......】

就連卡特莉娜的也，剛出生的小孩子們全員具有魔法使的素養。相飯，現在反而產生不安了。

接生的時候總是發光，所以霍恩海姆紅衣主教派來的助產婦和神官們都已經習慣了。

至於她們什麼都沒有往外說，我想是派遣她們過來的霍恩海姆紅衣主教一開始就下了封口令的緣故吧。

取而代之的是教會偶爾向外界洩露情報。

在這之後對方會採用怎樣的行動呢，這樣一想又變得不安了。

【作為下任維克爾的當家，凱恩能成為魔法使我很高興......】

這樣子總是生下來魔法使，各種各樣的辛苦會接踵而至，卡特莉娜這樣擔心著。

這種情況下辛苦的事情是強行把人推給我做妻子吧。

【這裡的話堅決拒絶就行了嘛】

我可不是三代冠軍世家啊。

被當做種馬怎麼能受得了！

【是啊，那三人的話沒辦法的了呢。】

【額！】

卡特莉娜指出來阿格尼絲她們的事情，我的心裡好像被什麼刺痛了的樣子。

然而，現在她們還是接受我指導的弟子。

年齡上像是妹妹一樣的存在，不贊成她們做妻子。

【不論如何，眼前都有要迫切解決的問題。】

【迫切的問題？】

【是的，貴族家的孩子出生的話，會有各種各樣的麻煩的。】

羅德里希看到了我的孩子都委託了的樣子，其實今後的威脅更大。

【得到了祝賀的都發送感謝信了嗎？】

【恩，基本是這樣的。】

總而言之，以第八個孩子為分界線寶寶潮結束了，然後此時，鮑邁斯特伯爵家捲進了各種各樣的騷動之中。

