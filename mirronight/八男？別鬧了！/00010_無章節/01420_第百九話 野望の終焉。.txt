　野望の終焉。

1.

「大魔砲，終於完成了呢！

快點開始進行作戰吧」

包含我在內有許多人參加了巨大決戰兵器『大魔砲』的製造，製造期間用了快一個月才完成。

這段期間，彼得似乎忙於統治著包含紐倫堡公爵領在內的南部領域，以及對紐倫堡公爵所窩藏的地下要塞進行包圍，針對試圖秘密將情報和物資搬入的敵方地下勢力的戰鬥這些事情上。

除此之外，紐倫堡公爵領的領民們則都老實地接受了帝國的統治。

但是，那些舉動只要這座地下要塞不攻下來就不會持續下去。

如果因軍費負擔難以承受而彼得決定撤軍的話，轉眼間紐倫堡公爵就會從地下要塞內出來再次控制住南部。

但是，地下要塞被堅固的『魔法障壁』覆蓋著。

為了破壞那東西的『大魔砲』終於完成了。

砲管的直徑有一米，長度二十米，其他構造還有支撐它的砲架、瞄準器、冷卻裝置、蓄積魔晶石的連結裝置、輔助動力傳輸系統的露易絲這些東西。(註:上一話是用原文的砲身，這話改成"砲管"。貼吧版的只能將錯就錯)

「把僕的拳頭所積存起來的魔力當成輔助動力，是相當突兀的構造吧？」

當成砲管材料的『極限鋼』，是我精心用魔法合成出來的。

因為把許多複雜的金屬和稀土類進行調配給保密下來了，這或許只有有著地球知識的我才做得出來。

多虧這樣，使我陷入迫於想要知道那套調配比例的彼得和瑞穗上級伯爵的壓力了。

但是，就算知道了以目前的爐子也是無法進行精製的。(註:其實有兩點:1.很耗魔力且要一次完成的精密作業，2.就算要用爐子也要用反射爐這個時代並沒有這種東西)

煉製技術的問題，要把這些材料均勻地混合再一起是非常困難的。

「威爾，今後會忙於『極限鋼』了呢」

「說不定喔」

明明混入的奧裡哈康和秘銀不多，但這些金屬和耐久性、硬度、魔力傳導的相性不差。

當成武器的素材，會是最好的金屬。

「只不過，『極限鋼』和其他金屬的加工方式雖然一樣，但工具一定要是奧裡哈康製的」

還有，我認為鑽石也可以。

因為在這個世界上被當成珠寶飾品和魔道具的材料來使用，但研磨或切削成珠寶飾品不要的碎鑽需求會提高也不一定。

總之，這個金屬的前途一片光明。

不對，因為這東西在古代魔法文明時代就有了，所以算是古代技術的復活吧。

回到王國後，說不定會接到來自陛下的委託。

「那部分的考量之後再說，把露易絲當成輔助動力的說法也太奇怪了吧」

「雖然被當成裝置看待了，但也只是站著握住這東西而已」

露易絲，只使用一隻手拿著連接砲架的傳輸線站在一旁罷了。

「使用之前的秘奧義注入到傳輸線上嗎？」

「是在更之前的狀態，把大量的魔力蓄積在拳頭上的狀態。好像是僕可以積存超過自己數倍容量的魔力而被選上的樣子」

露易絲的身旁，有著集滿大量魔晶石的袋子。

從這些東西上吸取魔力再集中至拳頭上，然後把魔力透過傳輸線傳送到大魔砲，對此露易絲做了說明。

「最新技術，被整合進去的僕就成了怪異的魔砲了」

和露易絲一起把視線轉向瞄準器那邊時，維爾瑪正在瞄準器那裡進行最後的調整。

我也和她打了聲招呼。

「維爾瑪，沒問題吧？」

「試驗過好幾次了所以是沒問題的。只是比狙擊魔銃的瞄準器還要大罷了」

「幾乎不需要有狙擊魔銃那樣的精確度吧？」

「大致上，這東西能打中就行了」

露易絲是從設置在大魔砲上的瞄準器觀看的，而讓我看的是克萊姆山脈和山腰的詳細地圖。

「紅色標記的地方，被設置了吐息產生裝置」

彼得這一個月都沒有在玩的樣子。

儘管將『魔法障壁』破壞了，但同時也要對能給予地下要塞的設備沉重打擊的地方進行調查。

「這種程度的瞄準，對維爾瑪來說很從容吧」

「並不能掉以輕心。第一次試射時會對瞄準器的誤差進行調整，所以沒問題」

「是嗎，加油喔」

我來回摸著維爾瑪的頭，使她露出了高興的表情。

「威爾，那僕呢？」

「誒？ 之前明明說過『僕是成熟的女人，所以即使被摸頭了也不會開心的』不是嗎？」

「有那回事嗎？ 喂，這很不公平喔」

在對那副模樣的露易絲摸頭的時候，她嘟囔了一句。

「好想快點回到鮑麥斯特伯爵領喔」

「是啊。這座大魔砲的攻擊成功的話，馬上就能回去了」

「失敗的話呢？」

「我想應該不會。要是失敗爆炸了，我也要讓留在這裡的維爾瑪和露易絲平安無事」

如果只有我和她們二個人的話，我有自信能夠用『魔法障壁』保護的。

關於其他人，並不需要照顧到那種程度。

切削砲管的兼真先生，『我不用魔法來保護。失敗的話，死我一個就行了』一起告訴我和弟子了。

總覺得，這種心理準備的做法或許跟古時候的日本工匠氣質很相似。

光這點，就代表有自信吧。

「有威爾在就放心了」

在我們三個人對話的時候，作業順利進行的同時來作戰開始的時刻也越來越近了。

這時的彼得把艾美菈放置在身旁，在指揮所內瞪視著地下要塞吧。

「準備發射的信號來了」

「維爾瑪，給予對方重重的一擊吧！」

「瞭解，會做得很漂亮的」

話是這麼說，也只是做好瞄準拉下發射桿而已，所以維爾瑪才能那麼冷靜。

「魔晶石連結裝置開始，開始供給魔力。現在、填充率百分之五十七」

製造起來最辛苦的魔晶石連接裝置，似乎順利地在運作了。

只是不斷在進行加熱，就連熱氣都傳到我們這裡來了。

說不定要長時間使用會很困難。

「填充率百分之一百五十」

「僕這邊，也要開始供給魔力了」

緊接著，露易絲也把拳頭上的魔力透過傳輸線輸送到大魔砲。

因為是輔助動力的角色，以性能第一確保這座大魔砲的威力不至於威力不足，所以才會需要她的力量。

「確認魔力已經填充了。瞄準，朝上二、右三修正。瞄準完畢。發射」

維爾瑪以淡然的聲音將所有的工作確認完成後，立刻拉下了發射桿。

一瞬間，大魔砲的周圍就好像被地震襲擊了一樣搖晃不已。

關於大魔砲的聲音方面，其實魔銃那點聲音只算的上是皮毛而已。(註:在88話就有提過，魔銃用的子彈底部有魔晶石，透過外部魔力進行起爆來發射子彈)

因為並不是靠火藥來引爆，所以不會發生鼓膜被震破的問題。

因為一般的大砲，發射的能量會全部在前方(砲口)被釋放出來。

因此對我們並不會帶來衝擊，但相反地為了抑制所產生的高溫在使用冷卻裝置時會不斷被大量的水蒸氣所襲擊。

包含我自己在內，用『魔法障壁』來為二人抵擋水蒸氣。

「水蒸氣真是驚人耶」

「和之前一樣」

和使用試作型大型狙撃魔銃的時後一樣，維爾瑪有穿著施有秘銀塗層的外套。

但是，比起上次這回水蒸氣的量更多了。

「命中了嗎？」

「好像」

我急忙用望遠鏡進行查看，確認到山脈的一部分被打出了一個相當大的洞來。

毫不留情地把那道『魔法障壁』打破了，(砲彈)似乎就那麼擊穿了山腰暴亂似的破壞了地下要塞內的設施。

既沒有引信也不是炸彈，只是在砲彈上裝了鎢合金的彈頭而已，那樣的質量被以大量的魔力加速後，就會產生很巨大的破壞力吧。

「好驚人的威力啊。維爾瑪，不斷進行射擊」

「明白了」

維爾瑪一邊看著地圖一邊開始進行瞄準。

在地圖山腰處，依序有標著要狙擊的地點。

這是，在彼得的命令下所調查到的成果。

「下一發裝填！ 好了！」

「咦？ 導師在做裝彈手？」

我製造出大略的形狀，再由工匠們進行修整的鎢合金砲彈出奇的沉重。

因此，雖然照計畫是由好幾名強化過身體能力的魔法使來進行裝填，但是不知為何正式上場時卻變成導師一個人在裝填砲彈。

「雖說多少用了魔法把身體能力強化了，但能拿的這麼輕鬆真不簡單」

「對某來說，這種程度算輕的」

因導師裝填彈藥的作業完成速度很快，才使得維爾瑪能不斷地以大魔砲進行射擊。

偏離目標是不可能的。

就算位置多少有些偏離，只要貫入山腰內的話確實會給地下基地帶來損害的。

「果然瞄準器會有誤差。修正方位下一、左一。發射」

我所製造出來的砲彈一共有二十五枚，想再做也沒有材料了。 (註:これ以上は材料がなくてどうにもならなかった。背後要讀者腦補打完就沒了)

雖然用其他材料也可以，反正蓄積在魔晶石上的魔力用盡了結果還是一樣。 (註:魔晶石に溜めていた魔力が盡きるので同じだ。同樣是跟上一句連動。而這邊的魔力用盡的是指輔助動力的露易絲和負責冷卻裝置的主角)

而且，如果被那種巨大砲彈連續打中二十五發的話，每次讓『魔法障壁』復原就需要用掉相當龐大的魔力。

山腰，可以看見許多地方出現大洞來了。

砲彈穿進去在洞穴內部翻騰，應該已經使得地下要塞破破爛爛了。

「全部的砲彈都命中了。我想吐息產生裝置也破壞得差不多了」

「好厲害啊。維爾瑪」

當在給完成任務的維爾瑪表揚之時，彼得那邊行動起來了。

被認為是先鋒部隊的帝國軍精銳，朝滿目瘡痍的山腰進攻了。

由於大魔砲的砲擊，讓叛亂軍那邊無法再維持『魔法障壁』了吧。

那件事，我們都確認過了。

「這樣一來，或許爭奪功名為優先已經開始了」

「但是，掉以輕心是要不得的吧」

艾爾對伊娜的發言提出反駁。

實際上，先遣部隊接近山腰時被安然無恙的吐息產生裝置攻擊了。

但是……。

「有艾美菈小姐在攻擊是無效的喔」

彼得，並不會犯兩次同樣的錯誤。

吐息完全被領在前頭的艾美菈抵擋住後，再由其他的魔法使們對著發射地點不斷地射擊使其沉默了。

「帝國的魔法使還很多啊。大概都是強行徵招來的吧……」

不管是數量或是質量看上去都很充足，這些吃虧的都是冒險者、內政負責人、魔法技術員才對。

為了快點回到原來的地方，彼得才會想快點攻佔這座地下要塞。

「伯爵大人，我們也行動吧」

「好吧。出發了」

「亞爾尼姆隊！ 一起衝進去吧！」

「希望不會太遲！」

在帝國軍的先遣隊開始侵入地下要塞的同時，我們也領著大軍前往地下要塞了。

人數隻有艾爾和遙所指揮的一千人而已，但侵入地下要塞前若是帶太多的兵力會難以指揮的。

「艾爾溫，別太過興奮放鬆下來喔」

「我和哥哥，會和瑞穗伯國軍一起行動」

要是總兵力七千人進行移動相反地會拖慢速度，率領剩下來的六千人的菲力普和克里斯多福，預定要和瑞穗伯國軍一起行動。

「進攻開始！」

我們把已經沒有彈藥可打的大魔砲交給瑞穗工匠後便以地下要塞為目標。

「威爾，要不要把紐倫堡公爵的頭當成目標？」

聽到露易絲問了件奇怪的事。

「頭沒用處。讓給想要的人去就好」

戰鬥也來到的最後階段，叛亂的首謀紐倫堡公爵雖然知道他的頭很有價值，但對身為外國貴族的我來說拿到他的頭只是增加麻煩而已。

以現代人的觀點來看，本身匯兌拿著頭顱的行為感到不舒服。

比起那件事，現在把裝置破壞掉才是最優先的。

「反正，會很搶手而沒有出手的機會吧」

「的確，不光是士兵連貴族都想要呢？」

「沒錯。只要成為法衣，就有子爵以上的爵位」

「對了。如果是我們這種外國人的話會給予鉅額的報酬吧。就算運氣好拿到頭顱，還要小心會被背後的我方捅一刀」

「在戰場上常常聽到呢。艾爾先生，我們踏實地前進吧」

「因為守護威爾就夠忙的了不會去做的喔。遙小姐」

艾爾和遙以及伊娜，好像想起了剛才彼得對帝國軍官兵們在進攻之前所說過的話了。

因為眾多的貴族沒落了，就算只有一個人拿到了紐倫堡公爵的頭對取得爵位來說是不痛不癢的。

「地下基地的規模相當大啊」

在山腰處往洞裡面窺伺時，可以看到裡面有露出一條地下要塞的通道。

果然是利用地下遺蹟的設施，路的形狀正是地下遺蹟的通道。

「通道意外的寬呢」

因為是魔闘流的使用者所以領在前頭尋找敵人氣息的露易絲，好像因地下遺蹟的寬闊而感動了。

「連在原本的古代魔法文明時代，都是軍隊或當什麼用途的地下基地吧」

布朗塔庫先生的推論很有道理。

全部都理解了，到目前為止紐倫堡公爵使用過的各式各樣魔道具都是先前從這裡到手的。

一般來說，一個地下遺蹟幾乎不會有那麼多的發掘品。

這裡保存的狀態很良好可說是奇蹟，這是個沒被挖掘到的巨大地下遺蹟吧。

2.

「我們要找的裝置會在哪裡呢？」

「正常來說，會是在地下最深處吧」

感覺那個地方會有最終的首領在，不過，得在其他人打歪念頭之前確保能將那個裝置破壞掉。

我們把後路交給艾爾所指揮的部隊，同時用相當快的速度移動到深處。

途中，彼得派出來的部隊和紐倫堡公爵的軍隊正陷入激烈衝突的場面，定時會聽到劍戟的交鳴聲。

「有敵軍！」

「抱歉，時間寶貴」

當我發動『範圍擊暈』時，朝我們這裡突進過來的敵方部隊全部都因麻痺而不能動了。

為了節省魔力並沒對威力進行微調。

就算觸電而死，我想也只能怪戰爭吧。

「依舊很無情喏」

泰蕾莎，一邊看著麻痺不能動的敵軍士兵一邊陳述了感想。

「真是的，明明很危險，不跟來比較好吧」

「待在溫德林的身旁是最安全的。而且，也從彼德殿那邊接要親眼確認溫德林有把那像裝置給確實破壞掉的工作。妾身也會一點劍術。自己的身體可以自己守護請安心吧」

在敵軍出現大量的麻痺者之時，我們潛入了地下要塞。

這時候已經來到很深的地方，已經聽不見我方人員在戰鬥的聲音了。

「好安靜啊」

「太奇怪了」

如露易絲的預感所料，樓梯下面的通道那邊果然有許多自爆型格雷姆，和吐息產生裝置被設置了。

「應對的方法已經確立了」

對一般士兵來說會很辛苦，但如果是魔法使的話就不是那麼困難的對手了。

自爆型格雷姆，受到一定程度以上的衝擊就會爆炸。

「鮑麥斯特伯爵。材料來了！」

導師打了地下遺蹟牆壁製造出大量的石塊出來，我則把那些石塊當成『石飛鏢』丟嚮往我們這邊過來的自爆型格雷姆。

於是，自爆型格雷姆產生爆炸了，飛過來的碎片用『魔法障壁』來抵擋就結束了。

「伊娜、維爾瑪」

「交給我」

「射擊！」

吐息產生裝置也一樣。

吐息裝置是量產型的機能上只能用在據點防衛上而無法移動。

伊娜從遠處進行投槍，維爾瑪則進行狙擊，那些攻擊全都用『提升』強化過所以輕易地就能破壞掉。

因為沒了吐息的機能失效了，所以龍格雷姆就很脆弱了。

「話說，已經進到相當裡面了卻還沒找到」

我們衝進來的目的就是想要找到那套裝置，但已經來到相當裡面了卻還是沒發現。

因為彼得他們有必要對地下要塞做詳細地調查，所以似乎還在比我們還要上面的樓層進行苦戰。

途中，明顯有士兵和他們的家人住的居住區正在興建中。

在這種地方一個不小心被發現的話會非常麻煩的，所以我們無視調這裡繼續往下一層移動了。

沒辦法過來的彼得，是因為攻佔和佔據很花時間的所以才沒辦法下來。

「滿是機關！ 紐倫堡公爵不相信任何人吧！」

每一層，都只設置了自爆型格雷姆和吐息產生裝置而已。

當將那些東西邊破壞邊前進時，看見了一道巨大的門，在它的前面則被設置了大量的吐息產生裝置。

當從遠處以魔法進行破壞時，在前方的導師一邊展開『魔法障壁』一邊進行接近，然後把那些東西全都用拳擊和腳踢破壞掉了。

往導師身上大量傾注過來的各種屬性吐息，全部都被他彈開了。

「一撃全壞！ 接連不斷還真麻煩啊！」

在巨大之門的前面，吐息產生裝置只剩下殘骸被留下了。

「那麼，這裡就是最深處的房間嗎？」

導師一個人打開了巨大的門。

然而，紐倫堡公爵和一位穿著白色燕尾服的奇怪大叔正在那裡等待著。

「紐倫堡公爵的重臣？

看起來不像啊……」

「伯爵大人，看看那傢伙的耳朵」

照著布朗塔庫先生的話，看見那位大叔的耳朵是尖的。

並不像精靈，應該說這個世界上不論是精靈或矮人都是幻想的種族。

實際上並不存在。

「魔族……」

布朗塔庫先生，簡潔地告訴我真相了。

外表和人類沒什麼差別，而特徵就是耳朵是尖的。

自從古代魔法文明時代崩毀後就沒有再目擊到的案例了，但可確定的是魔族是確實存在的。

真人，為什麼會去幫忙紐倫堡公爵啊。

「答案很清楚了！ 謎一樣的阻礙裝置也好、許多古代魔法文明時代的遺産也好。都是出自這位魔族的協助！」

導師，很罕見地對紐倫堡公爵氣的這麼厲害。

因為，原因只是紐倫堡公爵的叛亂還沒有結束。

說過要取得大陸霸權的紐倫堡公爵，其實這整件事是魔族之國的附帶條件吧。

更糟的是，在這位魔族之人的分化策略下，紐倫堡公爵是故意給帝國傷害的疑問浮上檯面了。

「有那麼壞嗎？」

「你！」

「能拿來利用的就用，反正最後我贏就行了。只要贏了所有的作為都會被正當化。事情就是如此不是嗎？」 (註:それが全てではないのかな。這句有不管我怎麼做只要贏了就是如此的意思

「什！」

對紐倫堡公爵的反駁，導師罕見地啞口無言了。

「話說回來，鮑麥斯特伯爵。幹得真是漂亮啊」

「哼……」

除了幹得很漂亮這句話外就沒再多說了。

為了破壞堅固的『魔法障壁』而將古代失落的『極限鋼』技術復活了，用這東西當材料製造出大魔砲。

托那東西的福那鐵壁般的『魔法障壁』才能被打破，加上用精銳進行突如其來的奇襲使得紐倫堡公爵家的部隊處在混亂之中。

而我們，則被允許侵入地下要塞的最深處。

「我也只是讓古代魔法文明時代的遺物復活復活而已。和在那邊的魔族所復活的物品相比算是微不足道吧？」

「就因那微不足道的東西，就把我籠城戰略給完全擊潰了」

現在，在地下要塞內兩軍正以超過二十萬的陣容在進行死鬥。

就算是紐倫堡公爵家的部隊都是精銳，在大魔砲的砲擊下還是產生混亂了，應該很難將突入到這地方來的大量帝國軍給排除掉。

「結果，對我來說鮑麥斯特伯爵正是我的罩門啊。很可惜只能把你殺了」

「非常瞧不起人啊。現在的你，能贏過我嗎？」 (註:目の上目線，這邊是用名詞的寫法。以居高臨下的態度和眼神在看人。字典裡面是"上から目線"或是"目上目線")

雖然曾經從泰蕾莎那邊聽過他是個劍術高手，如果是的話只要用魔法來打倒就行了。

我打算燒死他，便開始準備用『火炎』魔法。

然而，正當魔法快要完成的時候火焰卻完全消失了。

「被取消了嗎？」

「正確答案。身為魔族的吾輩擅長的魔法系統是『闇』。『闇』的強項，有著其他系統魔法所沒有的特殊性」

口吻和導師很相似的魔族，瞬間用闇魔法把我的魔法給取消了。

「這種魔法還能這麼用」

「嗚啊！」

「艾爾！」

隨即，突然間艾爾的身體被黑色的煙霧包圍住了，在它散開後的同時眼白的部分變黑的艾爾忘我似的斬向了泰蕾莎。

千鈞一髮之際伊娜介入進來了，以槍柄將艾爾的劍擋了下來。

3.

「艾爾！」

「艾爾先生！ 請振作起來！」

「嗚啊ーーー！」

遙也拔出魔刀來支援並且對艾爾進行喊話，但艾爾則沒有對二人的呼喚作回答，發出意義不明的叫聲同時發狂似的持續將刀揮落下來。

好像忘了到目前為止所學過的劍術，簡直就像狂戰士一樣。

「在操控對方的心嗎？」

「該怎麼說呢？那位少年只不過是把鮑麥斯特伯爵他們當成敵人來看」

「真是麻煩的魔法啊……」

要是複數的人被這種魔法的話，最糟會因同室操戈而全滅吧。

所有人繃緊著神經，但把那道危險給排除掉的是埃莉絲。

我們所在之處整個被青白色的光芒覆蓋了，等它散開來的時候艾爾就恢復原狀了。

「咦？ 我怎麼了？」

「艾爾先生，你被那位魔族操控了喔」

「噢噢喔！ 有與『闇』成對比『聖』魔法使用者啊！

吾輩，被那份實力感動了！」

看來，闇魔法在性質上能用聖魔法來抵銷。

只是需要一定程度以上的實力，因此魔族才會對埃莉絲的實力感到驚訝。

「魔族，你的王牌闇魔法好像沒用了吧。和紐倫堡公爵一起把頭交出來」

「突然就說要把人斬首，人類真是野蠻的種族啊」

「你啊……，在看到這個帝國的慘狀的時候才想到要讓人放過你嗎？」

和紐倫堡公爵一樣，不論是對帝國人的情感，還是法律上的考量都沒有死刑之外的刑責可以商量的。

如果在這裡被活捉，即使活著在痛苦的拷問後最終還是會面臨被殘忍殺害的下場。

這樣的話，還不如立刻死在在這裡還比較值得同情。

「鮑麥斯特伯爵真是溫柔啊」

「不對。光是要把你這種擁有這般魔力的人活捉，只會讓犧牲更多而已。只要能刺探出各種各樣的情報來的話對王國貴族來說就夠了，要饒你不死很奢侈啊。你就和大量屠殺的共犯紐倫堡公爵一起死吧」

「吾輩，身為考古學者只對遺蹟之類的東西感興趣而已」

「那種解釋聽過太多了」

「這句話早就有料到了。這樣一來，現在就和照顧吾輩的紐倫堡公爵站一起戰鬥。說不定還會有勝算」

話說完之後，魔族從燕尾服的內側口袋裡拿出了總覺得是四角形箱子的東西。

仔細一看，很像是遙控器之類的東西。

「這座地下遺蹟，是古代魔法文明時代某個國家的軍事兵器製造工廠和試作品的組裝廠。那些東西里面，也有珍藏的秘密武器。出來喲！

『巨人型機關魔人君』！」

在魔族按下遙控器上的按鈕後，紐倫堡公爵和魔族所站著的後方牆壁崩塌了，從那裡出現了一架全長近二十米的巨大人型的格雷姆。

「什麼啊。又是格雷姆啊」

「這架格雷姆，是把收集來的格雷姆缺點加以有效的運用，可是把那些缺點都解決掉了喔」

在魔族喋喋不休的時候，我、卡特莉娜、導師、布朗塔庫先生毫不留情地朝二人放出了魔法，但是那些全都被捨棄闇魔法而傾全力展開魔法障壁』的魔族彈開了。

「伯爵大人！ 那個魔族！」

「沒錯。魔力比我還要多」

到目前為止，幾乎沒有比我的魔力還要多的人存在，真不愧是魔族，有著值得讓人誇讚的驚人魔力。

「這架巨人型機關魔人君，是由紐倫堡公爵所操作的，在吾輩供給魔力後就能發揮出驚人的力量了」

「把你們都殺光之後，再去排除剩下的帝國軍吧」

紐倫堡公爵被魔族抱著的同時，靠著『飛翔』進入到格雷姆所打開來的胸部裡了。

那個地方是座艙的所在處。

「那個魔族，拿著師父所擁有過的『移動取消裝置』啊……」


明明我們仍然飛不起來，但那個魔族卻能使用『飛翔』。

也就是說，他持有那個魔道具。

「那麼，好好感受一下巨人型機關魔人君的強大而絕望吧」

從巨大格雷姆那傳來了搭乘在裡面的紐倫堡公爵之聲，聽起來就像是立體聲的廣播一樣。

駕駛艙裡面似乎有能把裡面的聲音往外頭擴音的裝置。

這裡明明是有魔法的西洋風格的幻想世界，不知為何對面那邊卻在上演機器人的動畫片。

會有這種想法的，大概就只有我而已吧。

「來試試武器吧」

紐倫堡公爵的心情就像得到了新玩具一樣。

在我們前方約五十米處的巨大格雷姆，它的雙手向前伸了。

然後，兩隻前臂就像火箭一樣朝我們飛過來。 (註:両腕の肘から下がロケット，白話一點就是金剛飛拳)

彷彿，就像小時候曾看過的機器人動畫中出現過的火箭拳。

「手臂在飛啊！」

「是用了『飛翔』的魔道具嗎？」

雖然艾爾和遙很吃驚，不過看樣子似乎沒時間逃了。

我和導師張開了堅固的『魔法障壁』，各把一邊火箭拳給擋下了。

「威力好驚人……」

「還沒停下來！」

就算用『魔法障壁』頂住了，但還是沒辦法將火箭拳給停下來還在持續突進想把我們給擊敗。

雖然被壓制到一直往後面退，不過再次將『魔法障壁』強化來進行防禦，才勉強地與對方陷入膠著的狀態。

從那之後過了十幾秒，火箭拳終於回到原本的地方去了。

「很不妙的武器啊」

「嗯」

布朗塔庫先生，對火箭拳的威力露出小心謹慎的感覺來了。

威力也很驚人，我和導師只能用『魔法障壁』來抵擋住貫穿而已只要火箭拳本身沒有損傷就能一直發射下去。

不辦法應付的話，只要『魔法障壁』被貫穿了就會讓我們遭到直擊吧。

「古代魔法文明時代的超兵器威力如何？

鮑麥斯特伯爵」

從巨大格雷姆那邊傳來了紐倫堡公爵自信滿滿的聲音。

只要這麼持續下去，好像就一定能打倒我們。

「那個臭小子，露出原本的面目了嗎？」

至今碰面時都是裝作一副紳士的模樣，不過似乎從相當程度的絕境之中感覺到有逆轉的可能性，所以聲音裡面才會洩漏出傲慢的一面。

「終於變回孩子時的馬克斯了嗎？

在要敵人面前扮演有容乃大的一面也辛苦他了」

在我身後，泰蕾莎一個人有所領悟了。

「泰蕾莎退下，其他王國軍組也是」

因為怕那種巨大的格雷姆會再次出現另一架，所以除了重要的戰鬥人員以外都只能從這個房間離開。

一般的士兵們很輕易就會被蹂躪了，在那之前(撤退)我們只能以戰鬥來拖住時間而已。

「艾爾」

「抱歉，我是不會撤退的。遙小姐，請和士兵們一起離開這個房間」

「艾爾先生！ 我也要留下來！」


「不行，要是妳現在參加了是會成為威爾厄運下的犧牲品喔。這次，還不能讓遙小姐參與」

「但是……」

「看在我的面子上。是不可能會輸的」

「……。我明白了」

身為瑞穗女性的遙，在聽完艾爾的說法後而下定決心隨兵撤離了。

「艾爾溫，我要留下來。畢竟我也是鮑麥斯特伯爵的護衛」

「不行。這場戰鬥威爾的護衛只有我一人喔。你應該去幫忙遙小姐吧？」

「我明白了，就交給艾爾溫你了」

剛臣先生，也對要和遙一起隨著士兵們從這個房間撤退一事能夠體諒了。

「艾爾溫，別死喔」

雖然是妹控，但剛臣先生似乎認可艾爾了。

罕見地，溫柔地和他聊著天。

「泰蕾莎大人。您也是」

「不要，妾身要留下來」

「可是，泰蕾莎大人您的劍術……」

並非魔法使，就連劍術的本領也不是很厲害。

況且留下來是很魯莽的，這是剛臣先生對泰蕾莎的見解。

「不認為溫德林會輸，即使妾身死了對帝國也不會有任何影響。再說，妾身也是有用處的」

泰蕾莎，不知從胸口那邊取出什麼東西來了。

仔細一看是魔法袋，從裡面取出來的東西好像在哪邊有看過，然後把上頭有插栓的部分拉開後朝巨大格雷姆那邊丟過去了。

「把眼睛閉上！」

在泰蕾莎的指示下所有人閉上了雙眼，同時那個物體就在巨大格雷姆的所在處發出閃光來了。

果然，那個物體是類似閃光手榴彈般的東西。

「閃光炸裂魔彈是有效的喏」

「噢噢ーーー！ 好刺眼啊ーーー！」

「泰蕾莎妳這傢伙！

眼睛被矇住了！」

再次把火箭拳發射出來的紐倫堡公爵他們，因閃光手榴彈使得視力暫時被奪走的關係陷入混亂的狀態了。

4.

「令人意想不到的暗牌啊」

「溫德林，古代魔法文明時代的遺産並不只有出現在紐倫堡公爵領而已。雖然數量很少，但這類的東西都還有在菲利浦公爵領流轉著」

泰蕾莎所拿的魔法袋，裡面收藏著過去在菲利浦公爵領內被發掘出來的魔道具之中特別貴重的危險物品。

「為什麼那東西，會被從家主引退下來的泰蕾莎拿著？」

「歷代的家主，都有義務保守這個魔法袋內之物的秘密。本來是必須交給阿爾馮斯的，但至今還沒做家主的交接之故所以失去轉交的機會。正好有機會。能夠在這裡使用到」

泰蕾莎，持續把某樣東西從魔法袋內拿出來。

大小約一米左右的筒狀物體，仔細一看那是類似反坦克火箭般的東西。

「這東西聽說叫做『魔導噴射砲』」

「會使用嗎？」

「閒暇之時，有讀過和這東西一起找到的說明書」

泰蕾莎，架起了魔導噴射砲並扣下板機。

被發射出去的噴射彈命中了巨大格雷姆的右肘，右側的火箭臂被扯下來了。

「威力真是強大喏」

「就是現在！」

紐倫堡公爵他們的視力還沒回覆過來的現在正是機會。

我、布朗塔庫先生、導師、卡特莉娜詠唱了魔法，艾爾和伊娜則用槍投擲，維爾瑪則是對巨大格雷姆的眼睛部位進行狙擊破壞。

肢體幾乎被破壞掉的巨大格雷姆，倒在地面上的同時發出了轟鳴聲。

「太好了！」

「不對，等等」

看到巨大格雷姆不能動的艾爾正高興著，但對魔法組的來說卻很清楚一件事。

那就是魔族的強大魔力還健在，不斷傳來的心跳聲似乎有什麼在蠢動著。

「只有這種程度ーーー！

是打倒不了巨人型機關魔人君的喔ーーー！ 過來吧(Come here)！」

當魔族發出喊叫聲的同時，巨大格雷姆損傷的頭和手足與身體分離開後便浮在半空中。

接著從後方被破壞掉的牆壁深處另一台巨大格雷姆出現了，把備用的手腳發射過來和前一台格雷姆進行合體。

巨大格雷姆，馬上恢復到原本的姿態了。

「天真真是天真，太過小看吾輩了啊ーーー。這幾年，吾輩都拚命地在進行發掘品的修理」

也就是說，有很多巨大格雷姆的零件，只要身體內充當內臟的魔晶石和魔族還有魔力就能持續運作，加上還有新零件可以更換不論多少次都能復活過來。

「某種意義上！ 這架巨人型機關魔人君是無敵的！」

「看到了吧鮑麥斯特伯爵！

這架巨人型機關魔人君的威力！」

不知不覺從妨礙視線攻擊中恢復過來的紐倫堡公爵，對我們大笑了。

那個模樣，就像是討人厭的最終首領。

「聽他在胡說八道。伯爵大人，只要多擊潰幾次零件也會用盡的吧」

「說的沒錯」

贊同布朗塔庫先生說法的我們，再次以全員進行攻擊使得巨大格雷姆因破破爛爛被打退了。

但是……。

「過來(Come here)吧ーーー！」

手腳和頭的備品再次飛來，巨大格雷姆恢復原貌了。

「溫德林先生，繼續喔」

「好，但以三次為限」

再一次，集中了魔法的砲火攻擊巨大格雷姆。

雖然再次變得破破爛爛，馬上又因新的手腳飛來而恢復原貌了。

「沒完沒了啊」

「真是遺憾啊，鮑麥斯特伯爵喲！

魔族喲！ 給他最後一擊！」

「那麼，攻擊再次開始！」

紐倫堡公爵，因巨大格雷姆的頑強更加有自信了。

火箭拳再次從巨大格雷姆的雙手飛出了，不知道什麼時候裝備在背上的背負式魔砲也在這次攻擊中將彈藥射出了。

火箭拳加上其他的砲擊，使我們變成防守方了。

在『魔法障壁』的展開下，魔法使們的魔力正慢慢地在減少當中。

「這下子，很不妙了不是嗎？」

「多破壞幾次的話，那架巨大格雷姆就不會復活了吧？」

一邊抵擋兩雙火箭臂加上魔砲的砲擊同時，我和導師在商討這下該怎麼辦才好。

「泰蕾莎，還有什麼秘密兵器嗎？」

「提到攻撃力，那東西和對方的魔砲沒有差別。那在之前問個問題可以嗎？」

「有什麼疑問嗎？」

「嗚嗯。比起攻擊格雷姆的本體，攻擊它後方把備品補充的部件破壞掉不是比較好嗎？」

「……是啊！」

這種理所當然的事情，直到我被泰蕾莎指謫之前都給忘了。

如果破壞格雷姆本身新零件就會飛過來的話，只要把發射零件的裝置破壞掉就行了。

「目標！ 巨大格雷姆後方那個會讓手腳飛過來的房間！」

「伯爵大人和導師不攻擊的話，只能靠我們來努力了吧……」

忙著應付火箭拳和炮擊的我和導師，是不參加這次攻擊的。

布朗塔庫先生和卡特莉娜射出大量的『火球』，露易絲和伊娜則是把魔力蓄積在槍上，泰蕾莎則連射著彈藥還有剩的魔導噴射砲。

被發射出去的魔法和彈藥從巨大格雷姆身旁穿了過去，就那麼鑽進它後方壞掉的牆壁上所打開來的洞內，隨後就產生大爆炸了。

「怎麼回事ーーー！ 合體系統啊ーーー！」

泰蕾莎的策略是正確的。

沒損壞的手足備品，都被爆炸捲進去而遭到破壞了。

「魔族！比起那個還有那裝置！」

「在那場爆炸之下有很高的可能性故障了。姑且還是先說一下這不是吾輩的錯」

「那個裝置？」

一閃而過的念頭詠唱『飛翔』的時候，我的身體漂浮在半空中了。

幾乎整整一年之久，我在經過這麼長的時間之後終於能夠飛了。

「那個裝置，最後的意外實在太沒意思了」

「伯爵大人，趁現在我們所有人持續攻擊吧」

「如果給予空隙讓他們復活過來也很困擾呢。全員攻撃開始！」

巨大格雷姆，因損壞零件的更換系統被破壞了要復活是不可能了。

這樣一來，更要趁現在徹底地破壞乾淨。

「一鼓作氣！ 用力ーーー！」

導師解開了『魔法障壁』並用魔法強化了身體機能，就那麼用雙手抓住火箭拳，像老虎鉗一樣緊緊地抓著。

導師毫無保留地使用了魔力進行著攻擊，那雙火箭拳漸漸地因被壓扁而出現裂痕了。

「伊娜！」

「艾爾！」

接著，二人將投擲用的槍投出。

槍打中了巨大格雷姆的火箭拳接合處，使得那個部分凹陷下去了。

這下子，火箭拳就無法再次合體了。

「接著是僕喔」

取回『飛翔』的露易絲，在被我用『魔法障壁』阻擋下來的火箭臂上像個雜技師般站在上面，然後把蓄滿強大魔力的一撃由上而下揮落了。

火箭臂成了四散的碎片落在地面上。

「伯爵大人！ 走吧！」

「好！」

見證火箭拳完全破壞後，我和布朗塔庫先生往巨大格雷姆跑去。

魔砲依舊持續不斷在攻擊，但是卡特莉娜操作了壓縮到極限威力大增的『風刃』繞到格雷姆的後方，將背上的魔砲砍下來了。

5.

「因為曾被師父大人提點過，魔法的控制能提升真是太好了」

被砍下來使得魔力供給遭到斷絕的魔砲，就那麼沉默了。

「魔族！ 想想辦法啊！」

「這就是俗話說的，大ーーー危ーーー機！」

「我要殺了你！」

「囉嗦、難看、沒有團隊精神。射擊」(註:這句是維爾瑪說的)

傳來了紐倫堡公爵慌亂的怒吼聲，填補上來的巨大格雷姆的雙眼遭到維爾瑪的狙擊被射穿了，使得他們的視野整個被奪走了。

「這時候，用吾輩的魔法……。嗚！」

「魔族！ 怎麼了！」

「身體不太能夠動啊。身體水腫，連頭也暈個不停」

「為什麼會這樣？ 鮑麥斯特伯爵的魔法嗎？」

「很抱歉，並不是我喔」

「是我」

一邊靜靜地應付著魔族的闇魔法這項工作的埃莉絲，同時開始著手以偷襲的方式進行反擊了。

因為魔族也是生物和人類一樣活用了治癒魔法能夠回覆的這項特質，一點點地給予增強，反覆地施放著治癒魔法。

就算是治癒魔法，一旦過度反而會造成危害。

埃莉絲，只從遠處瞄準魔族(將治癒魔法)射了過來，讓他的身體不斷浸透著高濃度治癒魔法的困難工作成功了。

「產生過度治癒狀態時，肌肉會浮腫、焦躁、上氣不接下氣、暈眩、對精神造成壞的影響。如果更加放任不管的話……」

埃莉絲對我們說著，最壞的情況是會死的。

「咦？ 之前我的治癒魔法太強了……」

「若是需求量只有數倍∼數十倍左右的話是不會有任何效果的。需求量必須要超過數百倍以上才行」(註:這句的意思是只施放一發特大號的治癒魔法是無法觸發過度治療的效果，而必須要多次不斷增加強度來進行觸發)

「這真是出人意料啊」

巨大格雷姆的損壞部位更換系統被破壞了，(魔族)自己也因過度治療的副作用讓身體感到不舒服。

魔族似乎相當脆弱的樣子。

能夠給予最後一擊的最佳機會，應該就是現在了。

「布朗塔庫先生！」

「噢！」

此時，和一直在保存魔力的布朗塔庫先生一起往巨大格雷姆那邊跑了過去。

「別讓他們過來！」

「驅使魔族還真是粗暴啊」

真不愧是魔族，只是受過度治療所造成的痛苦而已。

他使用了他那強大的魔力，有如風暴一樣將『風刃』展開了。

「所以我才會在這喔！」

只不過，那些全都被布朗塔庫先生展開的『魔法障壁』抵擋下來了。

「伯爵大人，那架巨大格雷姆的身體好像相當堅固該怎麼辦？」

布朗塔庫先生一邊展開著『魔法障壁』一邊前進的同時，我則在思考該用什麼魔法來讓那架巨大格雷姆無法戰鬥。

確實，不管給予怎樣的傷害四肢的部分姑且不說，光是有駕駛座的身體是無法給予傷害的。

「釋放魔法……」

威力很低，是無法對巨大格雷姆的身體部分造成傷害的。

那麼該怎麼辦才好呢？

答案，曾在之前和師父一戰時發現到。

「不把龐大的魔力放出來，將其收束在一點……。不對，這種情況是『一刀』吧……」(註:一刀，這裡帶有居合斬的意思。原意是將刀揮砍一次的意思)

取出了師父的遺物魔力劍的劍柄，讓它蓄滿著至今都沒有過的龐大魔力。

不過，要儘量讓具現化的刀身細長點。

長度最低限度至少要能把巨大格雷姆給劈開來，所以儘可能不能讓它變得太短。

是我的想像力所造成的問題嗎？

從劍柄現出來的是一把類似日本刀的紅色之刃。

因為是紅色所以是火系統，但看上去並不像有火焰般的物體存在。

而且，刀刃極細。

「用這個來燒斷」

用『飛翔』接近到巨大格雷姆的面前，一口氣把炎之刀身揮下了。

「就算是鮑麥斯特伯爵，這架巨大格雷姆的身體也是採用『極限鋼』和秘銀的合金複合裝甲喔。要斬開來是不可能的……什麼！」

從紐倫堡公爵那裡傳來了驚訝的聲音。

因為，巨大格雷姆的身體被斬開了，從裂縫那裡可以看見紐倫堡公爵的身影。

但是，還完全無法一刀兩斷。

明明把魔力收束到那種程度了，還只能把巨大格雷姆的前方裝甲給切開而已。

「再一次……」

雖然想這麼做，但好像把魔力使用過頭了。

我因感覺到暈眩當場坐下來了。

「伯爵大人」

「布朗塔庫先生，換你……」

「靠我的魔力，也只能造成奇怪的擦傷而已喔。導師！」

「很勉強。侵入到這裡，以及和巨大格雷姆為對手一戰魔力已經消耗到超乎想像的劇烈了」

「卡特莉娜小姑娘呢？」

「即使我把剩餘的魔力集結起來，也做不出像溫德林先生那樣的刀身」

「啊ーーー！」

地下遺蹟的最深處往這個房間來的路途上曾經歷過戰鬥，和巨大格雷姆戰鬥時所有人的殘存魔力就很讓人不安了。

如果是一般戰鬥的話是綽綽有餘，但要破壞巨大格雷姆的身體是不可能的。

「麻煩大了……」

巨大格雷姆的活動還沒有完全停止。

如果不快點給予最後一擊的話敵人的援軍有可能會到來，我開始在思考有什麼方法能破壞巨大格雷姆。

但是，那份擔憂被某個意外的人物給解決了。

「威爾！ 讓我來！」

「艾爾？」

「等等！你使用不了任何魔法的吧！」

因為對象是自己人，至今沒進行太多攻擊的艾爾打算突進時，卻被布朗塔庫先生急忙阻止下來了。

「有這東西啊！ 露易絲！」

「瞭解！ 如果艾爾不行的話，用僕和伊娜醬的投擲來做最後一擊就行了」

「能從遙小姐那邊把這東西借過來真是太好了！」

艾爾，拔出了從遙那邊借來的魔刀，然後讓提升到極限的火魔法纏繞在刀上面。

因為四肢被破壞掉的格雷姆還漂浮在半空中，到目前為止的移動都被露易絲強制終止了。

「心情就像投石機上的石頭啊」

「去吧！ 艾爾！」

被纏繞著魔力的露易絲拋出去的艾爾，對剛才我在格雷姆身體上製造出來的裂縫加以準確性的一擊了。

落地的艾爾馬上收起了魔刀，巨大的格雷姆外觀上並沒被砍到的印象。

「艾爾，沒有什麼變化嗎？」

「放心吧。那架巨大格雷姆已經變成兩半了」

艾爾自信滿滿地回答後沒多久，巨大格雷姆真的被縱向分成二半崩落了。

說到底，壞成這樣是沒辦法漂浮在半空中的。

落在地面上發出了咔噹的聲音，化成殘骸停止活動了。

「因為我說過。已經砍中了」

「誒誒！ 好厲害啊！」

最後的最後，給予最頑強的敵人最後一擊了。 (最後の最後で、一番の難敵に止めを刺した。)

艾爾，取得最關鍵的勝利了。

「艾爾！ 好厲害的一擊啊！」

「如果要說明原因的話，那架巨大格雷姆在受到威爾的一擊後已經出現很嚴重的損傷了」

雖然外觀只是把身體正面裝甲地的一部分給砍下來而已，實際上其他部分好像因看不見的傷痕變得破破爛爛了。

於是，可以說艾爾用魔刀加以一擊才觸使它崩壞的。

「因此才會變成兩半啊……」

大家使用了相當多的魔力，才讓那架頑強的巨大格雷姆倒下。

由於最堅固的身軀部分變成了二半，而崩落了。

「溫德林喲，不先確認那兩個人啊」

「對啊」

在泰蕾莎的指謫下急忙往格雷姆的殘骸山而去，尋找坐在格雷姆裡面的紐倫堡公爵和魔族。

首先一開始，就發現右手、右腳被砍下來大出血的紐倫堡公爵身影。

被我和艾爾的兩段所捲入，才使他的身體被砍成這樣的。

雖然勉強還有意識，但看到那樣的傷勢和出血量後連想要施救都沒辦法。

6.

「親愛的，可以用『奇蹟之光』……」

沒錯，埃莉絲的『奇蹟之光』算得上是意外。

問我能不能使用，然而在回答之前紐倫堡公爵先出聲了。

「現在的我並不需要被同情。就算用魔法治癒了，終究還是會在那個笨蛋皇帝三男的審判中被判處死刑的。這樣的話，還不如在這裡死的難看點還比較好」

「不……，但是……」

若是把將死之人放任不管是會產生罪惡感的，考慮到他還活著如果照彼得原本的意思所提出來的方案是不會動搖的，因此泰蕾莎才會陳述著自己的意見。

「好了。讓他就這麼死掉吧。叛亂的罪魁禍首應該會被曝首在帝都吶。從屍體上砍下來，或是讓以活著的方式砍頭處刑也是一樣的」

「泰蕾莎言之有理。但是，現在很謝謝你們」

我遵照泰蕾莎的建議，就這麼看著紐倫堡公爵死去。

「果然還是輸了啊。一開始在帝都暗殺泰蕾莎失敗的時候……然後，聽到她被鮑麥斯特伯爵所救時就有那種預感了」

紐倫堡公爵的口氣和平時一樣沒有改變，不過，卻因手腳的傷勢和大量出血露出痛苦的表情來了。

「至少，想要有個沒有痛苦的死亡」

看見他那樣子的埃莉絲，緊急將斷肢給堵住避免再出血。

因為流失的血是無法填補的所以還是會死的，但傷口的痛楚應該消失了。

「感謝。同情敵人這一點和聖女的稱號很相襯……。好羨慕啊，鮑麥斯特伯爵」

「嗯」

這種時候不知該怎麼回答比較好。

所以，用簡潔地一句話回答了。

「姑且不提一般人會羨慕你有好太太，但我可是很羨慕鮑麥斯特伯爵(你這個人)的喔」

「是嗎？」

雖然能使用魔法，卻因內心裡面有著老百姓一樣的優柔寡斷之故，我想(自己)才會被到處利用吧。

「貴族只要生下來是次男以下就會失去那個身份。我對那種悲哀不是很能理解。即使聽過也只是抽象般的理解而已，因為我是長男也是繼承人。明明只是獨子卻要我理解那樣的想法就很奇怪」

「這樣啊」

紐倫堡公爵的想說的事情，我能夠理解。

明明自己並非身處在那樣的立場，卻很清楚那種心情的人，就只是個偽善者而已。

「所以，我想在孩提之時就很羨慕那些成為冒險者而自由自在生活的人。對他們來說，如果對身為公爵家的繼承人的我說出那樣的事情時應該會很生氣吧……」


人，都會對自己沒有的東西產生慾望。

所謂『旁觀者清』是正確的嗎？

「(他)年幼時，曾和妾身玩起扮演冒險者的遊戲」

「當時還真是開心啊……，泰蕾莎扮演女劍士的角色……」

紐倫堡公爵的腦海裡，正浮現出曾在孩子的時候和泰蕾莎一起扮演冒險者的景象。

「但，畢竟我是紐倫堡公爵，泰蕾莎最後也成了菲利浦公爵。對我們來說，這就是血統的詛咒吧」 (註:血の呪い。原文是血之詛咒。血指的是貴族血統，從"青の血"衍伸而來)

「是啊。即使閉口不提也不能說出『很討厭，不想繼承』的話來。也不能對其他人說」

「雖然紐倫堡公爵家有一千二百年的歴史，但是，卻不是靠我自己創設的。也沒有滿腔的惜別之情喔。只是出於義務擔任起紐倫堡公爵這個身份罷了……」

有才能的紐倫堡公爵，在就任公爵後成為一位很好的執政者，在帝國的中樞也是作為一位軍事天才被期待在將來可以以這樣的立場率領帝國軍。

雖然被當成年輕的才子來期待了，不過，本人卻不是這麼想的。

因此，心理面逐漸地誕生出如黑暗一樣的東西。

「一位好的領主大人，還是個被期待的未來軍事指揮官。這樣的我受到了週遭之人的稱讚和羨慕，但是我完全高興不起來……。所以，才會有這種想法。如果是這樣的話，使用這份才能和地位總能做出一件大膽的舉動來的……」

反正，掌握了帝國再將王國攻滅剩下就是統一大陸了。

就是挑戰那種莽撞的夢想才會演變成這樣的結果吧。

「以這種想法行動起來的話就能稍微忘掉一些空虛了吧。卻沒料到會因此出現犧牲者。如果贏了即便是屠殺者我應該也會被稱讚。就算做出無謀的賭注而輸掉了也只會留下失敗的愚蠢者的評價。很快樂不是嗎」

「……」

不管是誰，都沒有人去指責紐倫堡公爵。

因為，大家都注意到了就算這個男人做出這種事情也不會有什麼效果的。(註:下一句開始的對話很重要，有興趣的朋友可以配合生肉去鑽研)

「我難看地戰敗而死，歷史悠久的紐倫堡公爵家將絕後了。明明泰蕾莎贏過我卻失去了菲利浦公爵的爵位和下任皇帝的寶座。真是諷刺啊……」

紐倫堡公爵，對著我露出了意義深長的微笑並且繼續和泰蕾莎說著話。

「是啊。現在的妾身被強制引退成為名譽伯爵了。得到能在帝國安養到老是確實的，身為為政者的彼得殿要是改變心意了就有可能被處理掉。算了，那種擔心在離開帝國後就沒必要了」

(注1:おれば，是"有"或"在"的意思。基本上是貶抑的用語。泰蕾莎這一整句在暗諷彼得)

(注2:飼い殺し，這個名詞在幕間14和布洛瓦篇已經解釋過了，請自行查閱。順便一提主角的父親處於這種下場)

「離開帝國？ 自己一個人自由自在的生活嗎？」

「並不是像平民一樣有著完整的自由，至少比在菲利浦公爵的時代還自由吧」

「這樣啊……」

「很驚訝居然會被彼得殿和溫德林拉下來變成一個只會打扮而無所事事的女人。但，就現在來看，這樣子反而還比較幸福」

泰蕾莎笑著和紐倫堡公爵說話的時候，他一瞬間露出羨慕般的表情了。

「真想看看泰蕾莎今後自由自在的生活啊……。幾十年後……來世再會……」

「是啊，再見了馬克斯」

話說這裡就結束了，紐倫堡公爵靜靜地閉上了雙眼。

竭盡最後的氣力努力地把話說下去，現在已經來到極限了。

「已經死了」

埃莉絲對呼吸和脈搏進行確認，正式認定紐倫堡公爵死了。

「自由嗎……，大笨蛋……」

泰蕾莎仰望著頭嘀咕著。

不這樣的話，我想流淚的表情就會暴露在我們面前了吧。

大貴族在眾人面前哭泣，是很不得體的行為。

雖然可以大聲哭出來，但從以前就習慣這樣了吧。

「如果討厭紐倫堡公爵的地位，如果自己做出辭退的舉動來的話……。不對，那些妾身也做不到。所以，不能批評馬克斯……。但是，就沒有其他的選擇了嗎？

你真的是一個笨蛋啊」

「那個。威爾」

「沒有……」

伊娜似乎有『泰蕾莎和馬克斯，彼此都對對方抱持著異性的好感嗎？』的想法，不過，我並不這麼認為。

要說是什麼的話其實是友情上的寄託，我猜測二個人都是年輕人卻都不想繼承選帝侯這樣的地位以及要忍受隨之而來的重責，這樣的同志情感。

「身為叛亂者說不定永遠都會被批評，難道就沒有其他的選項嗎？」

泰蕾莎眼眶含著淚持續地仰望著。

「認真過頭了吧」

但目前為止靜靜地傾聽的布朗塔庫先生突然吐露出自己的想法了。

「做不到像伯爵大人那樣的果斷，任何事情都交給任由自己的喜好來判斷也做不到……」

「布朗塔庫。還記得之前馬克斯曾說過溫德林是個天才？

妾身也贊同那句話的一部分。因為溫德林擁有魔法的才能，才能輕易地補足其他貴族的所不足的能力，即使把領地的營運交給其他人來做也不會出問題的吧。如果由妾身來做的話，可是會變成那對兄弟的魁儡的。紐倫堡公爵家是武力至上的家係，軍系的家臣權力比較強，一旦所有人沾染上軍事就很危險。不可能走鮑麥斯特伯爵領的路線喔」(註:其實這一整句在講羅德里西和主角，36話到55話作者就曾說得很明白了)

「是這樣啊……」

「也沒有世人所想的那麼簡單」

「沒錯。將紐倫堡公爵的遺體回收後，還有其他的工作要做喔」

那個裝置似乎壞掉了，不過應該還沒有完全被破壞乾淨吧。

另外，這裡也有大量的發掘品在沉睡的可能性。

這些，也必須要儘可能回收破壞掉才行。

「是啊。入手這些兵器群的彼得殿不保證不會瘋掉。人和人之間，真的無法瞭解彼此的內心的……」

戰鬥也結束了，艾爾把在房間外待命的士兵們召集過來，發出命令要他們把紐倫堡公爵的遺體放在擔架上運送出去。

7.

「那個。難道忘了敵人的首領所面臨的最後橋段了」

「忘記了？」

「魔族死了吧？」

「糟糕！ 竟然忘了！」

面對露易絲的指謫，我急忙對在巨大格雷姆的殘骸那邊進行搜索的士兵們下令了。

馬上就發現，瓦礫下方所出現的魔族身影。

只是水腫的很厲害，身體機能似乎也在麻痺中而無法正常活動。

「看樣子，沒受到什麼致命傷呢」

「魔族，真是結實啊……」

來自埃莉絲的報告，讓我對魔族的生命力強度感到驚訝了。

「可以的話希望幫吾輩治好」

「幫你治好，要是你再度胡鬧起來可是制不住你的。可以的話，就請這麼死掉吧」

這個魔族，還殘存著強大的魔力。

如果疏忽這一點就給予治療又使他胡鬧起來的話，要讓他再次陷入無法戰鬥的狀態是很困難的。

不對，有可能我們都會被殺掉吧。

「或許此刻給他最後一擊會比較穩當」

伊娜的意見，讓除了魔族之外的所有人都點頭了。

「嗚嗚……，鮑麥斯特伯爵殘忍地讓人感到意外啊……」

「才不想被你這個引發內亂，要承擔起大量屠殺的共犯這麼說的」

令人驚訝的是即使受到這樣的傷勢魔族還能喋喋不休。

而且，還對我說出很諷刺的話來。

「威爾大人」

當我在思考怎麼處置這位魔族的時候，被維爾瑪拉著長袍呼喚了。

「什麼事？」

「把這個魔族殺掉的話，不會和魔族之國引發問題嗎？」

「噢噢！ 沒錯啊」

對第六感很好的維爾瑪，讓我們之中對這件事最有感觸的泰蕾莎也察覺了。

「自己國家的國民被殺了的話，也不是沒有會把軍隊派過來的可能性」

國與國之間並沒有真正的朋友，說不定那件事被當成藉口來進攻的話事情就大條了。

「泰蕾莎小姐，這個魔族是內亂的共犯……」

「雖然那是事實，但不得不考慮會和魔族之國打起來……」

我也對卡特莉娜的意見表示贊同，但泰蕾莎卻冷靜地思考著和魔族之國產生衝突的危險性。

「明明內亂終於結束了，帝國卻再次得和魔族之國進行戰爭。不，這種情況王國也有可能被襲擊的。在兩國關係修復之前功過來的話就危險喏」

「一開始感覺到這個人的魔力，居然超越很厲害的溫德林先生，但每個魔族都有像他那樣的威脅性嗎？」

這些人裡面具有魔族相關知識的，就只有原菲利浦公爵的泰蕾莎吧。

和我依樣突然成為貴族的卡特莉娜，並沒有那方面的知識。

「據說魔族，每個人都持有相當程度的魔力喔」

所有的敵軍，都是強大的魔法使。

就算我方投入的人數比較少且可以打倒某種數量的敵人，只要我們的魔力耗盡也會被殺的，還有不能使用魔法的士兵只會被蹂躪而已。

確實，並沒辦法選擇把他殺了的選項。

「導師，該怎麼辦呢？」

「喂，魔族！ 可以幫你治好，但是不推薦你進行抵抗！」

總覺得，導師好像不喜歡這個與自己的說話口氣很像的魔族。

只是，自己的感受和政治上的選擇是不同的吧。

才會選擇幫助他的選項。

「赫爾姆特王國的魔法使們，都是謹慎派的啊。就算此時能在與你們一戰中逃走，還是會被在上面樓層的帝國軍士兵們和魔法使們捉住而死的吧。吾輩，有不做白費功夫之事的特質」

「話還真多……。埃莉絲，幫他治療」

「是的。伯父大人」

在埃莉絲輕輕地施以治癒魔法後，讓因過度治癒魔法的關係而受到傷害的魔族恢復過來之後站起來了。

由於過度治療而受到傷害的身體，用了治癒魔法中不會殘下永久後遺症特性的治癒魔法而恢復過來的吧。

這是這種過度治癒的狀態不容易被利用的原因之一。

如此一來，用一般魔法來進行攻擊的作法效率上還比較好。

「很感謝妳的治療。吾輩，恩斯特・布立茲，是魔族之國的考古學者」 (註:アーネスト・ブリッツ/ Ernest・Blitz。アーネスト這個名字音譯是"歐內斯特"，這邊使用的是偏名。另外Blitz，有興趣的朋友可以自行百科，總之這個名字很有意思)

「考古學者？」

像謎一樣穿著白色燕尾服的中年男性，以自己是考古學者來做自我介紹。

「正是如此。吾輩的人生目標，是探索和調查未知的地下遺蹟」

有對魔族詢問起一些事情，他自豪地說自己是從魔族之國的西之島一個人以偷渡的方式潛入進大陸的。

偷渡入境後，聽到紐倫堡公爵領內有許多未發掘的遺蹟傳聞，於是就承包了希望能到那裡進行探索並把發掘品儘可能地做修理。

說起來，和他終究只是利益交換的關係。

「利益交換啊……」

「付出了這麼多的犧牲，這話還真是相當自私呢」

原本就是孤單一人，在我們之中堅持自己理念最深的卡特莉娜和導師，都對這位魔族感到驚訝了。

「雖然是這麼說，因為吾輩是考古學者所以會想要進行地下遺蹟的探索和調查的。因此，就有必須得到身為領主的紐倫堡公爵的許可」

魔族，也有魔道具工匠和研究人員的才能。

所以，才會把紐倫堡公爵家軍所裝備的發掘品修理到能夠使用。

「讓匕首能夠使用，即是把刀子交給擁有者後他殺了人也和吾輩一點關係都沒有」

「你！ 在開什麼玩笑啊！」

「艾爾先生，請冷靜一點！」

魔族太過義正嚴詞，使得艾爾憤怒到極點了。

遙急忙過來阻止。

「即使那麼憤怒，吾輩是靠那樣的契約在紐倫堡公爵領內的遺蹟做自由調查的」

我瞭解了。

他，打從心底就是個學者。

行動只是為了滿足自己的求知慾，並沒有考慮到會對週遭造成麻煩。

雖然還沒有到達瘋狂科學家的程度，但說不定是學者的典型。

而且，這種人正因欠缺某些東西才能得到成果的。

遵照和紐倫堡公爵的契約修理地下遺蹟所得到的東西，使得這些東西在戰場上威勢驚人。

雖然參加了內亂，終究是紐倫堡公爵的個人所為。

在巨大格雷姆的戰鬥也是出於自衛的想法……不，考量到他的來歷還是無法輕易進行處刑的。

「夠了。如果逃走或是對我們造成危害，到時候再圍毆他就行了。先探索這個最深層的房間吧。請帶路」

「交給吾輩。首先，這道牆壁雖然被刻上了裝飾用的花紋，這在古代魔法文明時代的後期很常見……」

「不是讓妳進行考古學的解說」

魔族，突然間開始對地下遺蹟牆壁上的花紋解釋起來了。

現在並沒有意義，急忙把話題誘導到發掘品上。

「發掘品方面嗎？ 調查已經結束了那些東西並沒有看見有任何學術上的價值」

「對身為考古學者的你是如此，但對活在這個現實的世界上的人來說就有必要找出它們的價值再做處置」

「吾輩明白了」

首先，在魔族的帶領下前往因巨大格雷姆打壞的牆壁而出現的房間裡面。

在那裡，有個總高度約十五米象是混合了巨大天和和打字機的裝置被設置了。

「這個，就是『移動』和『通信』的阻礙裝置？」

因為有泰蕾莎的忠告，並用魔法攻擊這裡真是太好了。

天線的一部分被打壞了，才無法發射出妨礙電波。。

「直接說可以吧？」

「可以。這種魔道具」

阻礙我唯一擅長的魔法的裝置，並沒有特意留下來的價值。

「弄壞也沒關係，不過先讓吾輩去拿之前使用過的零件」

高度魔道具是許多零件的集合體，也有很多可以挪用到其他魔道具上使用的零件。

突然間要我們先別破壞，魔族給予忠告等他先把那個零件拿出來後再破壞。

「特別是，這個裝置所使用的魔晶石很巨大啊」

「你是用這樣給裝置提供魔力的嗎？」

「吾輩，本職是調查與分析，還有對發掘品進行修理的工作。所以，吾輩哪裡都去不了只能待在這個裝置的旁邊。為了是對巨大的魔晶石注入魔力讓裝置發動」

魔族打開了裝設在裝置背後的艙門，裡面被設置了巨大的魔晶石。

8.

「早點破壞掉吧……」

在我的命令下，艾爾率領士兵開始把最底層裡面的魔道具回收了。

被破壞掉的吐息產生裝置和巨大格雷姆的殘骸，還有其他零件和不明的試作品格雷姆，還把看起來很像武器的東西收集過來了。

「可以進行技術解說和修理喔」

「那就好」

我，把收集過來的東西全都放入魔法袋內。

原本就會被當成戰利品來處理，雖說是在帝國領地內但我也有份。

內亂結束後，從紐倫堡公爵領接受到的魔道具如果讓彼得的帝國軍變得太強的話，下次就有可能和王國發生戰爭，因此這些東西全部都沒收了。

還有……。

「恩斯特・布利茲。就不能想辦法變裝一下嗎？」

「真是感動能記住吾輩的名字。可以變裝喔」

「那麼，打扮成人類」

不能把這個魔族交給彼得。

這傢伙只要能發掘未被盜挖的地下遺蹟就很滿足了，為此就得和壞人攜手合作。

所以，回到王國有必要藏匿在鮑麥斯特領後而且詢問一下魔族的相關情報。

距離現在將近一萬年，這塊大陸不曾見過魔族了。

雖是那樣如果我(光明正大)帶著魔族的話，很快其他的內幕就會見光死了。(註:這邊指的是把魔族帶回自己的領地，以及暗槓魔道具這件事)

「古代魔法文明時代，有存在一個能夠變裝的魔道具」

恩斯特從口袋取出一顆戒指，然後戴在手指上。

然後，就變裝成一個隨處可見的中年男性士兵了。

「這樣就安全了。只不過，有個唯一的擔憂」

「擔憂？」

「沒錯，因為很擔憂」

恩斯特，把目光看向泰蕾莎。

雖然是名譽職，畢竟就怕還是帝國貴族的泰蕾莎會透露給彼得知道，馬上就會暴露曾和他說過話一事。

「那樣的擔心是不必要的喏。妾身會離開帝國，本來就是個下任皇帝寶座被彼得殿給搶走之深。連一絲人情都不存」

「原來如此，可以理解」

我們在最底層的善後結束後，便一邊宣傳紐倫堡公爵已死的事情一邊還在持續做激鬥的地下要塞內奔跑著。

特別是，紐倫堡公爵家宅邸所在的地方，他那些狂熱的家臣和士兵們以及設置在那個地方殘存的自爆型格雷姆和吐息產生裝置還在奮戰中，給予攻過去的帝國軍相當大的犧牲了。

「投降吧！ 你們的領袖已經死了！」

「說那什麼無聊的謊言啊！

領主大人前往最底層去拿新兵器了！」

對於我的投降勸告，負責在宅邸周邊守備的重臣面露怒色地回答著。

對紐倫堡公爵死了這件事，固執地不願承認吧。

「看看這個！」

在我的指示下，紐倫堡公爵的遺體被抬出來了。

雖然也有拿出砍下來的頭來讓人觀看的意見，不過，如果那麼做的話可能使他們更有可能感情用事。

所以，才會這麼將遺體拿出來示眾。

「領主大人！」

「假的吧！很相似的冒牌貨而已！」

「沒時間了！ 這是真的紐倫堡公爵！

他的野望就此結束了。領頭羊不在的話成就野心就不會實現了。期望被留下來的你們有風骨一點」

「……怎麼辦？」

「戰到最後連一兵一卒都不剩！」

「那麼，做那種事情有什麼意義？」

在我的說服下，殘存下來的叛亂軍指揮官和士兵們因意見分歧開始在爭吵。

然後，最上級的人終於做下決定了。

「事以至此。我們投降……」

這之後還持續移動到各地進行說服，地下要塞的攻略作戰從開始經過十八小時後的現在，所有的叛亂軍都投降了使得戰鬥得以結束。

帝國軍的戰死者共計七千八百五十七名，叛亂軍的死者有五千六百七十八名。

雖然彼此都在戰鬥中付出了大量的犧牲，正因如此才讓帝國的內亂終於結束了。

聖誕節紀念

這是溫德林成年之前的故事。

________________________________________________________________

1.

「聖誕節馬上就要到了吧……」

不知為何轉生到有著和地球幾乎一模一樣曆法的這個世界接近十年了，雖然已經十四歲了，但如今的我不知道為什麼想起了日本的聖誕節。

2.

在我的前世名為一宮信吾的小時候，(聖誕節時)會在家裡吃蛋糕和烤雞，還會從打扮成聖誕老公公的父親那裡收到禮物。

國中和高中時期，即使在朋友和社團中都會舉辦聖誕派對。

在沒什麼女人緣的男性朋友中，(我是)最早和女友一起慶祝聖誕節的所以在收到同學和朋友的詛咒同時還引起了騷動。

成為大學生的我也有女朋友，還會為了買聖誕節的禮物而勤奮地打工。

雖然在畢業之前就被甩了，不過，我覺得那也是很好的回憶吧。

就業後，任職於主要是在經營以食品為主的貿易公司裡擔任後台的工作人員。

總之後台的工作人員，關於食品的相關工作傾向會在年末將形成記載下來，早則一年前，最晚也會在幾個月前就行動起來。

製作蛋糕的食品製造商和西點店，會因『蛋糕的材料不足所以無法再做出來』而吃不到。

事前必須要的東西，一定要提早計算出一定的數量並提前訂貨。

我們，正是在批發這些東西所以很忙碌。

即使聖誕節的腳步越來越近，計算也很瘋狂因此很常引發『這個不夠，那個也不夠』的麻煩出來。

雖然用『我們也沒辦法』來處理也可以，但像我們這種二流的公司正因能夠靠著準確的下單來應對，所以現實才有辦法與大公司相抗衡。

因為有可能隔年以後的生意會受到阻礙，所以從聖誕節開始到年末就得不停來回奔走忙碌著。

儘管如此，還好正月時能夠休息。

我真的覺得，有人可以一整年不停地在做生意真是厲害。

等等，說起來為什麼會突然想起這些事情呢……。

「鮑麥斯特男爵！ 今年也來到『狂歡節』的季節了！」

因為，會被導師強迫參加在這個時期中所舉辦的麻煩活動。

3.

「啊啊，煩死了啦……」

「真的……」

艾爾也和我一樣發著牢騷，那也是因為這個『狂歡節』的錯。

其實這種風俗的活動，只有在王都周邊有而已。


這麼說來，小時候為了批發獵物有前往布萊希堡過，不過，並沒有聽說過狂歡節的事情。

關於我的老家就更不用說了。

在鮑麥斯特騎士爵領內，大體上不存在結婚典禮、葬禮以及豐收節以外的活動。

「是教會辦的活動嗎？」

「僕也不曾聽說過」

連在布萊希堡長大的伊娜和露易絲，都不知道這個名為狂歡節的活動。

如果是教會的活動，在布萊希堡的教會進行時應該也會注意到才對，但是好像並沒有的樣子。

「這個活動呢。至今五百年前，由教會所認定聖人瓦倫丁樞機卿，為了將肉分贈給孤兒和貧民區的人們所進過的活動」(註:就是隔壁吧狂想曲中，賽菈在教會外煮飯的那種奉獻活動)

「類似煮飯賑濟的活動嗎？」

教會，會定期到孤兒院和貧民區煮飯賑濟。

伊娜向埃莉絲詢問，是不是類似的活動呢。

「基本上是一樣的，不過，是在有狩獵樞機卿之稱的瓦倫丁樞機卿的生日當天所舉行的，特徵是能痛快地享用獵物的肉這點喔」

擅長狩獵的樞機卿不知是怎麼想的，不過，在這個世界除了我以外誰都不會去在意。

他生日的這天和地球的聖誕節很接近，身為神官的他並不是靠募來的捐款去施捨給貧苦的人們，而是自己拿的弓箭去狩獵。

該說他是相當健壯嗎，好像是一位很積極的人。

「瓦倫丁輸機卿，是一位狩獵高手」

「（不出錢，只是動動身體……好痛！）」

像是沒在聽埃莉絲講話的艾爾不只發現問題點還去吐槽，對此我靜靜地給予一發肘擊。

人類，也有就算是事實也不能說出來的情況。

「問題在於狂歡節這個命名是怎麼來的吧？」

印象中是在歐洲舉辦化裝遊行和點心招待的節日，但在這個世界卻是志工將狩獵到的獵物烹煮起來宴請孤兒和貧民的活動。

志工是神官、自願參加的貴族、冒險者、家境富裕自願參加的平民，會趁有空的時候去狩獵的人們。

從以瓦倫丁樞機卿為名的活動開始來看，沒有情人節這種活動，雖然時期是在聖誕節但我們並沒有什麼能吃的。

只有在王都周邊有舉行，可以聚集能來參加人數的說不定也只有在這裡而已。

「埃莉絲，去年就有這種活動了嗎？」

「那是當然的，因為每年都會辦，不過，去年溫德霖大人很忙所以……」

每次來王都都會被許多大人們所折騰，生日也一樣光是被許多的貴族包圍著就累了。

所以，埃莉絲說就是因為這樣霍恩海姆樞機卿才沒有前來邀請。

「因為大家都很忙沒有每年都來也沒關係，不過，僑居在王都教會中有一定富裕程度的信徒，還是每幾年來參加會比較好」

並不是義務，但我是教會的名譽司祭所以好像得定期露露面。

「如果是威爾的話。捐款就可以解決不是很好嗎？」

「不，因為這是狂歡節……」

不管多了不起的貴族大人，也只有在這種狂歡節中會親自狩獵並烹煮得來的獵物，然後宴請貧民的活動最為重要。

「嗯……是在作秀嗎？」

「露易絲，說得太過直白了……」

捐款越多也會有讓人越感激的感覺，但是去參加狂歡節好像是一種政治作秀。

舉辦人，向期待者親自進行奉獻活動。

因此，不親自參加就沒意義了。

這樣的行動是可以理解的。

即使在日本，都有很多會在意週遭目光的而進行那種事情的有錢人和政治家、藝人。

「事情我都明白了，但就只有導師莫名地在高興啊」

艾爾，一邊看著沒有雜念地邀請我們去狩獵的導師一邊露出了不可思議的表情。

總覺得他很期待，能去參加這種活動。

「伯父大人，姑且每年這個活動他都會參加喔」

因為他是王宮的首席魔導師，光是參加就很顯眼了。

反正，王城裡那些麻煩要死的書類工作都交給部下在做。

連在狂歡節時都能偷懶到這種地步，不如認為他能替這節日在宣傳還比較好。

「某很喜歡狩獵！ 不管是動物或是魔物，可以狩獵到飽是人類的本能！」

「哪有啊……」

在看見導師毫無雜念在做參加狂歡節準備的同時，我們所有人都這麼認為了。

「「「「「（瓦倫丁樞機卿和導師，一定是很相似的人吧……）」」」」」

對於這點我們確信著。

4.

「威爾，捕獲到很多呢」

「就算捕獵到這麼多，我們連一塊碎肉都吃不到」

「回到宅邸後請期待那一頓的飯菜吧」

「好吧……」

狂歡節從一大早就開始進行著。

為了這一天，平時要對捕獵到的物品徵收規費的狩獵場變成了免費開放，而平時禁止進入的獵場也被開放，使得大家能夠獲得大量的獵物。(註:獲物，這邊是翻獵物，也能翻成戰利品)

到傍晚為止都在努力狩獵的我、艾爾、伊娜、露易絲、導師雖然得到了某種成就感，但狂歡節的正場才正要開始。

烹煮捕獵到的獵物，必須要分發給孤兒和貧民。

為此，埃莉絲在狩獵上是派不上用處的，因此和我們分開行動而去參加在教會本部內的烹調作業。

「溫德林大人，大豐收呢」

「還好啦」

我們，也一起幫忙有關烹煮獵物之外的處理了。

放血、取下毛皮。

把分切開來的肉在煮好或烤好後，免費地分發給聚集而來的人們。

「隨著聖人瓦倫丁機卿起了個頭，將血肉分送給所有人們的這個儀式……」

在分發給人們肉料理的旁邊，年老的樞機卿像是在問候一樣地說著話雖然不明白到底在說什麼，但，幾乎沒有任何人在聽。

對前來參加的人來說，能理所當然地得到免費的肉料理比較重要。

「說來，布朗塔庫先生沒來啊」

「去年，在布萊萊德邊境伯強制下令下有過來參加喔」

「今年不會來吧？」

「大概吧」

布萊希萊德邊境伯家因為在王都有房子，所以每年都會由家臣中的某人前來參加狂歡節的樣子。

去年，好像就是因為布朗塔庫先生當班的關係。

「那還真可憐……唔！」

因為艾爾又脫口說出多於之事的關係，我才會賞了一發肘擊給他讓他閉嘴繼續進行分配肉料理的作業。

「料理雖然量非常多，但是來排隊的人也很多呢」

「好累啊……」

伊娜和露易絲，似乎因從一大早開始就相當忙碌在狩獵、烹調以及配膳上而顯得很疲勞。

當然，我和艾爾也一樣。


雖然和聖誕節很類似，光是聽到重疊著的日期裡有活動就讓人有些欣喜雀躍了，只不過工作再認真也不能吃到飯。 (註:碌に飯も食えない，好好地吃飯都不行。這邊有作詞意修改，因為是發給窮人吃的主角他們不能碰，照著翻會有語意上的錯誤)

我，心裡只想著『這種山寨聖誕節，不想再參加第二次了！』。

「到這裡，料理的配膳結束了」

好不容易準備好的料理都沒了的時候，時間已是晚上了。

「到此就結束了吧」

「溫德林大人，還剩下後夜祭喔」

收拾完之後，我就這麼被埃莉絲告知了。

說到『後夜祭』，可以推測是和高中校慶結束後所舉行的慶功宴一樣的活動。

「（原來如此，因為要弄到這麼晚所以為了慰勞所舉行的招待）」

不管怎麼看，從早到晚一頓飯都沒有就很異常了。

辛苦到這種程度，還真感謝教會能端出飯菜出來。

「有飯吃就可以了。吃得飽就好」

「有沒有甜點呢？」

「雖然不能給未成年喝酒，但期待能有甜點出現呢」

就在艾爾、伊娜和露易絲快要達到與我的結論相同而在高興的時候，就被埃莉絲給阻止下來了。

「不對，從現在開始直到日期變換為止，教會本部會舉辦說故事大會……」

「誒？」

看來並不是辦派對，而是直到半夜陸陸續續會有教會的和尚們出來做感激似的說教。 (註:教會の坊主達，這是作者故意的。這邊的"坊主"是雙關語，一邊罵人不知變通一邊罵對方的長相。不知道中世紀神職人員頭髮造型的朋友，可以去看純潔的瑪麗亞)

「飯呢？」

「當然會端出微薄之物的」

這種時候，埃莉絲所說的微薄不是謊言可以明白的是粗茶淡飯。

從早上到半夜，只是遵照教會的指示不斷地勞動，之後還要去聽和尚說教。

即使在地球，嚴格的基督徒在聖誕節的派對中也不會去參加彌撒的，不過，狂歡節應該也是一樣的吧。

「那個……」

雖然我不想參加，但『討厭』也說不出來。

因為埃莉絲滿是要去參加的感覺，所以連逃走都辦不到了。

「溫德林大人，偶爾參加這種活動也不錯喔」

埃莉絲滿臉的笑容。

她，是打從心底這麼認為才對著我說的。

而且，平時的埃莉絲並不會說出要我去參加教會的活動。

幾年一次，再者我如果把生活據點搬到布萊希堡來的話，說不定就有永遠不用參加活動的可能了，因此現在考慮到週遭之人的觀感後更應該去參加。

雖然我很明白溫柔的她的用心，但是我還是不太想參加。

畢竟，我只是個尋常的普通人罷了。

「走吧……」

「是」

和埃莉絲一起朝後夜祭的會場大聖堂而去，都到這種時候了卻還有想逃的人。

「我有急事……」

「我，得去做吃飯的準備」

「僕也是，要去幫忙伊娜醬」

雖然艾爾他們打算逃走，不過，我當然是不會放跑他們的。

用魔法做出『繩』，然後將他們三人給拘束起來。

「你們真的以為逃得掉嗎？

來吧，我們一起去聽說教囉」

「威爾當代表不就可以了」

「威爾和未婚妻的埃莉絲二個人一起去平衡感就很高了」

「對呀對呀，伊娜醬說得沒錯」

對我而言，是真的想逃走的。

只不過，也沒有放過艾爾他們的理由。

但是，還是有一個人順利地逃掉了。

「導師不在嗎？」

「對……伯父大人，到目前為止一次都沒有參加過後夜祭……」

原來如此，很喜歡狩獵，以及會做秀地陪伴賑濟完，但是堅決不參加跟活動沒關聯性的後夜祭。

我對導師的難以應付，打從心底感到佩服。

5.

「聖書中第七節三項的故事，神之使徒海威爾(ハイウェル)幫助了倒在路上旅人，但就在這時候……」

被強制參加的後夜祭，真的無聊得要死。

姑且是有吃到飯，但卻只有很硬的黑麵包一個以及葡萄果汁而已。

在教會的教義中，黑麵包是肉，而葡萄汁就如同是血一樣的東西。

雖然得到了一個有關教會的知識，但不認為這能提供什麼好處。

光只吃這些根本是不夠的所以肚子一直叫個不停，但是這些偉大的和尚們談話不結束，我們就回不了家和吃到飯。

從剛才開始，神和祂的使徒們等真的有些可疑的故事，和過去聖人們的言行都在被神官們敘述著。 (註:神官達によって語られる，這一句就在告訴大家前面的"坊主"是故意的)

參加者，都靜靜地在聽著。

坦白說，不懂這到底有什麼有趣的。

仔細一看，半數以上的參加者眼神都像死魚一樣了。

從一大早就開始參加，加上並不是特別喜歡才來參加的，會覺得累是理所當然的。

其中有一部分的人，眼神閃耀著光輝在聽講。

是信仰的重症患者吧，我懷抱著壞心眼的想法，認為這種人容易受邪教所騙。

「（肚子餓了啦……唔庫！）」

「（安靜）」

我讓艾爾吃了第三發肘擊，讓他集中精神聽著無聊的演說。

結果，後夜祭結束之時日期已經移往隔一天了。

「日本的聖誕節！ 你們大半都不是基督徒，只有這種輕浮的聚會嗎請來說聲對不起！」(註:這句跟下面的敘述同指主角沒轉生前的描述，對話框應該是作者放錯)

大學畢業前被女朋友給甩了，就職後就都沒有正經地慶祝過聖誕節，所以對日本式的聖誕節很不爽而口出惡言一事感到抱歉。

蛋糕、雞肉、香檳、禮物。

輕浮的聖誕節，我比任何人都還要喜歡。 (註:浮わついたクリスマス，指的是非宗教性質的慶祝行為)

日後，我為了讓日本式的聖誕節做為後夜祭在世間普及開來，而向阿爾提裡歐先生做出指示了。

